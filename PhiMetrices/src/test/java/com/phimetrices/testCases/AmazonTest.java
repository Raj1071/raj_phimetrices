package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.AmazonBL;
import com.phimetrices.driver.CreateThread;

public class AmazonTest extends CreateThread {
	AmazonBL amazonLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability("appActivity",
				"com.amazon.mShop.splashscreen.StartupActivity");
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.Dashboard");
		
		
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.WebviewActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		amazonLib = new AmazonBL();
		reports.startTest("Amazon Test");
	}
	
	@Test(priority = 1)
	  public void shopProduct() throws Exception {
		amazonLib.shopProduct();
	
	 }
}
