package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.GoogleMapBL;
import com.phimetrices.driver.CreateThread;

public class GoogleMapTest extends CreateThread{
	GoogleMapBL googleMapLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.google.android.apps.maps");
		capabilities.setCapability("appActivity",
				"com.google.android.maps.MapsActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		googleMapLib = new GoogleMapBL();
		reports.startTest("Google Map Test");
	}

	@Test(priority = 1)
	public void loadingTime() throws Exception {
		googleMapLib.loadingTime();
		// driver.quit();
	}
}
