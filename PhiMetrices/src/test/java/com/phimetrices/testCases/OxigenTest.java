package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.OxigenBL;
import com.phimetrices.driver.CreateThread;

public class OxigenTest extends CreateThread{

	OxigenBL oxigenLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.oxigen.oxigenwallet");
		
		capabilities.setCapability("appActivity",
				"com.oxigen.oxigenwallet.Splash");
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.Dashboard");
		
		
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.WebviewActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		oxigenLib = new OxigenBL();
		reports.startTest("Oxigen Test");
	}
	
//	@Test(priority = 1)
//	  public void transferMoney() throws Exception {
//		oxigenLib.transferMoney();
//	
//	 }
	
	@Test(priority = 2)
	  public void loadMoney() throws Exception {
		oxigenLib.loadMoney();
	 }
}
