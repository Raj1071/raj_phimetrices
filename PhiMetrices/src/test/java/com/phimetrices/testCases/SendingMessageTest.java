package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



import com.phimetrices.businessLogic.SendingMessageBL;
import com.phimetrices.driver.CreateThread;

public class SendingMessageTest extends CreateThread {
	
	SendingMessageBL smsLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.android.mms");
		capabilities.setCapability("appActivity",
				"com.android.mms.ui.ComposeMessageActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		smsLib = new SendingMessageBL();
		reports.startTest("SMS sending Test");
	}

	 @Test(priority = 1)
	 public void sendingSMS() throws Exception {
		 smsLib.sendingSMS();
		 driver.quit();
	 }

}
