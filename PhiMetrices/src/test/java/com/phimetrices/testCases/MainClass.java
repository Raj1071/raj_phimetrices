package com.phimetrices.testCases;

import java.lang.reflect.Method;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

public class MainClass extends Activity{


    /*
     * Some significant methods for LTE: getLteSignalStrength, getLteRssi,
     * getLteRsrp and getRsrq. RSRP provides information about signal strength
     * and RSSI helps in determining interference and noise information. RSRQ
     * (Reference Signal Receive Quality) measurement and calculation is based
     * on both RSRP and RSSI.
     */

    private SignalStrength      signalStrength;
    private TelephonyManager    telephonyManager;
    private final static String LTE_TAG             = "LTE_Tag";
    private final static String LTE_SIGNAL_STRENGTH = "getLteSignalStrength";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        // Listener for the signal strength.
        final PhoneStateListener mListener = new PhoneStateListener()
        {
            @Override
            public void onSignalStrengthsChanged(SignalStrength sStrength)
            {
                signalStrength = sStrength;
                getLTEsignalStrength();
            }
        };

        // Register the listener for the telephony manager
        telephonyManager.listen(mListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    private void getLTEsignalStrength()
    {
            try
            {
                Method[] methods = android.telephony.SignalStrength.class.getMethods();

                for (Method mthd : methods)
                {
                    if (mthd.getName().equals(LTE_SIGNAL_STRENGTH))
                    {
                        int LTEsignalStrength = (Integer) mthd.invoke(signalStrength, new Object[] {});
                        Log.i(LTE_TAG, "signalStrength = " + LTEsignalStrength);
                        System.out.println("----------------------"+LTEsignalStrength+"------------------------");
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Log.e(LTE_TAG, "Exception: " + e.toString());
            }

           
    }
    

}
