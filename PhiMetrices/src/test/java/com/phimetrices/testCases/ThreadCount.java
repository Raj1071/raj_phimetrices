package com.phimetrices.testCases;

import utils.load.dataSource.ExcelLib;

public class ThreadCount {
	String sheetName = "Twitter";
	ExcelLib excelData = new ExcelLib();
	String fileName = "ResultSheet.xls";
	 private static int rowCount;
	    private static final Object countLock = new Object();

	    public void incrementCount() throws Exception{
	    	rowCount = excelData.getRowCount(sheetName, fileName);
	        synchronized (countLock) {
	        	rowCount++;
	        }
	    }
}
