package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.businessLogic.DropBoxBL;
import com.phimetrices.businessLogic.InstagramBL;
import com.phimetrices.businessLogic.MobileLocationFinderBL;
import com.phimetrices.driver.CreateThread;

public class DropBoxTest extends CreateThread {

	DropBoxBL dropBoxLib;
	MobileLocationFinderBL locationLib;
	String sheetName = "Dropbox";
	int rowCount;
	ExcelLib excelData = new ExcelLib();
	String fileName = "ResultSheet.xls";
	@BeforeClass
	public void configClass() throws Exception {
		rowCount =  excelData.getRowCount(sheetName, fileName);
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.dropbox.android");
		capabilities.setCapability("appActivity",
				"com.dropbox.android.activity.DropboxBrowser");
		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		dropBoxLib = new DropBoxBL();
		reports.startTest("DropBox Test");
	}

//	 @Test(priority = 1)
//	 public void login() throws Exception {
//	 dropBoxLib.login();
//	 }

	@Test(priority = 1)
	public void uploadFile() throws Exception {
		dropBoxLib.uploadFile(rowCount);
		driver.quit();
	}
	
	@AfterClass
	public void signalValue() throws Exception {
		System.out
				.println("------------------------In After Class--------------------------");

		capabilities.setCapability("appPackage", "com.AndLocation.AndLocation");
		capabilities.setCapability("appActivity",
				"com.AndLocation.AndLocation.AndLocationActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		locationLib = new MobileLocationFinderBL();
		reports.startTest("Mobile Location  Test");

		locationLib.getLATValue(sheetName, 6, 7,rowCount);
		driver.quit();
	}
}
