package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.UberBL;
import com.phimetrices.driver.CreateThread;

public class UberTest extends CreateThread{
	
	UberBL uberLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.ubercab");
		capabilities.setCapability("appActivity",
				"com.ubercab.UBUberActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		uberLib = new UberBL();
		reports.startTest("Uber Test");
	}

	 @Test(priority = 1)
	 public void bookRide() throws Exception {
		 uberLib.bookRide();
		 driver.quit();
	 }

}
