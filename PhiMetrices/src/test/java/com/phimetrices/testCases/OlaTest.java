package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.OlaBL;

import com.phimetrices.driver.CreateThread;

public class OlaTest extends CreateThread {
	OlaBL olaLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.olacabs.customer");
		capabilities.setCapability("appActivity",
				"com.olacabs.customer.ui.SplashActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		olaLib = new OlaBL();
		reports.startTest("Ola Test");
	}

	@Test(priority = 1)
	public void bookRide() throws Exception {
		olaLib.bookRide();
		// driver.quit();
	}
}
