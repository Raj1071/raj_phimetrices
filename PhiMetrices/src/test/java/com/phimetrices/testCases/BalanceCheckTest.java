package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.BalanceCheckBL;
import com.phimetrices.driver.CreateThread;

public class BalanceCheckTest extends CreateThread{
	BalanceCheckBL balanceCheckLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.android.dialer");
		capabilities.setCapability("appActivity",
				"com.android.dialer.DialtactsActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		balanceCheckLib = new BalanceCheckBL();
		reports.startTest("Balance Enquiry Test");
	}

	 @Test(priority = 1)
	 public void balanceCheck() throws Exception {
		 balanceCheckLib.balanceCheck();
		 driver.quit();
	 }
}
