package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.businessLogic.LocationAppBL;
import com.phimetrices.businessLogic.MobileLocationFinderBL;
import com.phimetrices.businessLogic.TwitterBL;
import com.phimetrices.businessLogic.WhatsAppBL;
import com.phimetrices.driver.CreateThread;

public class WhatsappTest extends CreateThread{
	WhatsAppBL whatsappLib;
	LocationAppBL signalsLib;
	String sheetName="Whatsapp";
	int rowCount;
	ExcelLib excelData = new ExcelLib();
	String fileName = "ResultSheet.xls";
	@BeforeClass
	public void configClass() throws Exception {
		rowCount =  excelData.getRowCount(sheetName, fileName);
		excelData.setExcelData(sheetName, rowCount, 0, rowCount + "",
				threadName, "ResultSheet.xls");
		excelData.setExcelData(sheetName, rowCount, 11, threadName + "", threadName,
				"ResultSheet.xls");
		System.out.println("------------------------In Before Class--------------------------");

		 capabilities.setCapability("appPackage", "com.whatsapp");
		 capabilities.setCapability("appActivity", "com.whatsapp.HomeActivity");
		
		 System.out
			.println("-------------------initDriver Method--------------------------");
	System.out.println("appiumServiceUrl ---"+appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");
		
			try {
				driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
						capabilities);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		whatsappLib = new WhatsAppBL();
		reports.startTest("Whatsapp Test");
	}

	@Test(priority = 1)
	public void sendImage() throws Exception {
		whatsappLib.sendImage(rowCount);
	}
//	
	@Test(priority = 2)
	public void sendVideo() throws Exception {
		whatsappLib.sendVideo(rowCount);
		driver.quit();
	}
	
//	@Test(priority = 3)
//	public void downloadImage() throws Exception {
//		whatsappLib.downloadImage(rowCount);
//	}
//	
//	@Test(priority = 4)
//	public void downloadVideo() throws Exception {
//		whatsappLib.downloadVideo(rowCount);
//	}
	
	@AfterClass
	public void signalValue() throws Exception {
		System.out
				.println("------------------------In After Class--------------------------");

		capabilities.setCapability("appPackage", "com.demo.locationtest");
		capabilities.setCapability("appActivity",
				"com.demo.locationtest.MainActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


		 signalsLib = new LocationAppBL();
			
			reports.startTest("LocationTest App Test execution");
		
			signalsLib.getSignalsValues(sheetName,12, 14,rowCount,13);
		driver.quit();
	}
}
