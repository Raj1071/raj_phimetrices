package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.businessLogic.FaceBookBL;
import com.phimetrices.businessLogic.LocationAppBL;
import com.phimetrices.driver.CreateThread;

public class FacebookTest extends CreateThread {

	FaceBookBL fbLib;
	LocationAppBL signalsLib;
	ExcelLib excelData = new ExcelLib();
	String sheetName = "Facebook";
	String fileName = "ResultSheet.xls";
	int rowCount;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@BeforeClass
	public void configClass() throws Exception {
		rowCount = excelData.getRowCount(sheetName, fileName);
		excelData.setExcelData(sheetName, rowCount, 0, rowCount + "",
				threadName, "ResultSheet.xls");
		excelData.setExcelData(sheetName, rowCount, 10, threadName + "", threadName,
				"ResultSheet.xls");
		System.out
				.println("------------------------In Before Class--------------------------");
		
		
		System.out.println("-----------rowCount Values---------"+rowCount);

		capabilities.setCapability("appPackage", "com.facebook.katana");
		capabilities.setCapability("appActivity",
				"com.facebook.katana.LoginActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
			Thread.sleep(4000);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		fbLib = new FaceBookBL();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Date beforeFeedDate = new Date();
		System.out.println("---------------Before feed time-----------"
				+ dateFormat.format(beforeFeedDate));

		WebElement validateFB = driver.findElement(By
				.id("com.facebook.katana:id/search_edit_text"));
		if (validateFB.isDisplayed()) {
			Date afterFeedDate = new Date();
			long diff = afterFeedDate.getTime() - beforeFeedDate.getTime();
			System.out.println("--------------Time fraction of wall feed---"
					+ diff);
			try {
				excelData.setExcelData("Facebook", rowCount, 2, diff + "",
						threadName, "ResultSheet.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		reports.startTest("Facebook Test");
	}

	// @Test(priority = 1)
	// public void login() throws Exception {
	// fbLib.login();
	// }

	@Test(priority = 1)
	public void postStatus() throws Exception {

		fbLib.postStatus(rowCount);

		// driver.quit();
	}

	@Test(priority = 2)
	public void uploadImage() throws Exception {
		fbLib.uploadImage(rowCount);
	}

	@Test(priority = 3)
	public void uploadVideo() throws Exception {
		fbLib.uploadVideo(rowCount);
		driver.quit();
	}

	// @Test(priority = 3)
	// public void logOut() throws Exception {
	// fbLib.logOut();
	// Thread.sleep(5000);
	// driver.quit();
	// }

	@AfterClass
	public void signalValue() throws Exception {
		System.out
				.println("------------------------In After Class--------------------------");

		capabilities.setCapability("appPackage", "com.demo.locationtest");
		capabilities.setCapability("appActivity",
				"com.demo.locationtest.MainActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		signalsLib = new LocationAppBL();

		reports.startTest("LocationTest App Test execution");

		signalsLib.getSignalsValues(sheetName, 11, 13, rowCount, 12);
		driver.quit();
	}

}
