package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.PaytmBL;
import com.phimetrices.driver.CreateThread;

public class PaytmTest extends CreateThread{

	PaytmBL paytmLib;

	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "net.one97.paytm");
		capabilities.setCapability("appActivity",
				"net.one97.paytm.AJRJarvisSplash");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		paytmLib = new PaytmBL();
		reports.startTest("Paytm Test");
	}
	
	@Test(priority = 1)
	  public void transferMoney() throws Exception {
		paytmLib.transferMoney();
	
	 }
//	@Test(priority = 2)
//	  public void addMoney() throws Exception {
//		paytmLib.addMoney();
//	 }
}
