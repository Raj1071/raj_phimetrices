package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.businessLogic.LocationAppBL;
import com.phimetrices.businessLogic.TwitterBL;
import com.phimetrices.driver.CreateThread;

public class TwitterTest extends CreateThread {

	TwitterBL twitterLib;
	LocationAppBL signalsLib;
	String sheetName = "Twitter";
	ExcelLib excelData = new ExcelLib();
	String fileName = "ResultSheet.xls";
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	int rowCount;

	@BeforeClass
	public void configClass() throws Exception {
		rowCount = excelData.getRowCount(sheetName, fileName);
		excelData.setExcelData(sheetName, rowCount, 0, rowCount + "",
				threadName, "ResultSheet.xls");
		excelData.setExcelData(sheetName, rowCount, 6, threadName + "", threadName,
				"ResultSheet.xls");
		System.out.println("-----------------row value this time---For Thread"
				+ threadName + " is " + rowCount);
		System.out.println("-----Before rowCount---" + rowCount);
		System.out
				.println("------------------------In Before Class--------------------------");
		capabilities.setCapability("appPackage", "com.twitter.android");
		capabilities.setCapability("appActivity",
				"com.twitter.android.LoginActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
			Thread.sleep(5000);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		twitterLib = new TwitterBL();
		Date beforeFeedDate = new Date();
		System.out.println("---------------Before feed time-----------"
				+ dateFormat.format(beforeFeedDate));
		WebElement validateTwitter = driver.findElement(By
				.id("com.twitter.android:id/home"));
		if (validateTwitter.isDisplayed()) {
			Date afterFeedDate = new Date();
			long diff = afterFeedDate.getTime() - beforeFeedDate.getTime();
			System.out.println("--------------Time fraction of wall feed---"
					+ diff);
			try {
				// excelData.setExcelData(sheetName, rowCount, 0, rowCount + "",
				// threadName, "ResultSheet.xls");
				excelData.setExcelData(sheetName, rowCount, 2, diff + "",
						threadName, "ResultSheet.xls");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		System.out.println("-----After rowCount---" + rowCount);
		reports.startTest("Twitter Test");
	}

	//
	// @Test(priority = 1)
	// public void login() throws Exception {
	// twitterLib.login();
	// }

	@Test(priority = 2)
	public synchronized void postStatus() throws Exception {
		System.out
				.println("-----------------------Twitter execution----------------------");
		// twitterLib.scrollWall();
		
		twitterLib.postStatus(rowCount);
		driver.quit();
	}

	// @Test(priority = 3)
	// public void logout() throws Exception {
	// twitterLib.logout();
	// }

	 @AfterClass
	 public void signalValue() throws Exception {
	 System.out
	 .println("------------------------In After Class--------------------------");
	
	 capabilities.setCapability("appPackage", "com.demo.locationtest");
	 capabilities.setCapability("appActivity",
	 "com.demo.locationtest.MainActivity");
	
	 System.out
	 .println("-------------------initDriver Method--------------------------");
	 System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);
	
	 System.out
	 .println("------------------------driver invoked----------------------------");
	
	 try {
	 driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
	 capabilities);
	 } catch (MalformedURLException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
	 }
	 // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	 signalsLib = new LocationAppBL();
	
	 reports.startTest("LocationTest App Test execution");
	
	 signalsLib.getSignalsValues(sheetName, 7, 9, rowCount, 8);
	
	 driver.quit();
	 }
}
