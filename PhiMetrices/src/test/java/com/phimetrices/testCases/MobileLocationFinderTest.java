package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.MobileLocationFinderBL;
import com.phimetrices.driver.CreateThread;

public class MobileLocationFinderTest extends CreateThread{

	MobileLocationFinderBL MobileLocationFinderLib;
	
	@BeforeClass
	public void configClass() {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.AndLocation.AndLocation");
		
		capabilities.setCapability("appActivity",
				"com.AndLocation.AndLocation.AndLocationActivity");
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.Dashboard");
		
		
//		capabilities.setCapability("appActivity",
//				"com.oxigen.oxigenwallet.WebviewActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		MobileLocationFinderLib = new MobileLocationFinderBL();
		reports.startTest("MobileLocationFinder Test");
	}
	
//	@Test(priority = 1)
//	  public void getLATValue() throws Exception {
//		MobileLocationFinderLib.getLATValue();
//		driver.quit();
//	 }
	
}
