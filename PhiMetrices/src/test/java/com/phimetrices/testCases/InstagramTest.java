package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.businessLogic.InstagramBL;
import com.phimetrices.businessLogic.LocationAppBL;
import com.phimetrices.driver.CreateThread;

public class InstagramTest extends CreateThread {
	InstagramBL instagramLib;
	LocationAppBL signalsLib;
	String sheetName = "Instagram";
	ExcelLib excelData = new ExcelLib();
	String fileName = "ResultSheet.xls";
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	volatile int rowCount;

	@BeforeClass
	public void configClass() throws Exception {
		rowCount = excelData.getRowCount(sheetName, fileName);
System.out.println("****************Device Name*************"+threadName+".......");
		excelData.setExcelData(sheetName, rowCount, 0, rowCount + "",
				threadName, "ResultSheet.xls");
		excelData.setExcelData(sheetName, rowCount, 6, threadName + "",
				threadName, "ResultSheet.xls");
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.instagram.android");
		capabilities.setCapability("appActivity",
				"com.instagram.android.activity.MainTabActivity");


		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
			Thread.sleep(3000);
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		instagramLib = new InstagramBL();
		Date beforeFeedDate = new Date();
		System.out.println("---------------Before feed time-----------"
				+ dateFormat.format(beforeFeedDate));
		WebElement validateInstagram = driver.findElement(By
				.xpath("//android.widget.ImageView[@content-desc='Camera']"));
		if (validateInstagram.isDisplayed()) {
			Date afterFeedDate = new Date();
			long diff = afterFeedDate.getTime() - beforeFeedDate.getTime();
			System.out.println("--------------Time fraction of wall feed---"
					+ diff);
			try {
				excelData.setExcelData(sheetName, rowCount, 2, diff + "",
						threadName, "ResultSheet.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		reports.startTest("Instagram Test");
	}

	// @Test(priority = 1)
	// public void login() throws Exception {
	// instagramLib.login();
	// }
	@Test(priority = 1)
	public void sendImage() throws Exception {
		System.out
		.println("------------------------In Test Class--------------------------");
		
		System.out.println("****************Device Name*************"+threadName+".......");
		instagramLib.uploadImage(rowCount);
		driver.quit();
	}

	// @Test(priority = 3)
	// public void logOut() throws Exception {
	// instagramLib.logOut();
	// }

	@AfterClass
	public void signalValue() throws Exception {
		System.out.println("****************Device Name*************"+threadName+".......");
		System.out
				.println("------------------------In After Class--------------------------");

		capabilities.setCapability("appPackage", "com.demo.locationtest");
		capabilities.setCapability("appActivity",
				"com.demo.locationtest.MainActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		signalsLib = new LocationAppBL();

		reports.startTest("LocationTest App Test execution");

		signalsLib.getSignalsValues(sheetName, 7, 9, rowCount, 8);
		driver.quit();
	}
}
