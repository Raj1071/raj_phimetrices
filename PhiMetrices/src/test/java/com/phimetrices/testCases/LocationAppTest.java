package com.phimetrices.testCases;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.phimetrices.businessLogic.LocationAppBL;
import com.phimetrices.driver.CreateThread;

public class LocationAppTest extends CreateThread{
	
	LocationAppBL signalsLib;
	
	@BeforeClass
	public void configClass() throws Exception {
		System.out
				.println("------------------------In Before Class--------------------------");

		capabilities.setCapability("appPackage", "com.demo.locationtest");
		capabilities.setCapability("appActivity",
				"com.demo.locationtest.MainActivity");

		System.out
				.println("-------------------initDriver Method--------------------------");
		System.out.println("appiumServiceUrl ---" + appiumServiceUrl1);

		System.out
				.println("------------------------driver invoked----------------------------");

		try {
			driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl1),
					capabilities);
			Thread.sleep(3000);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		signalsLib = new LocationAppBL();
		
		reports.startTest("LocationTest App Test execution");
	}
	
//	@Test(priority = 1)
//	public void postStatus() throws Exception {
//		System.out
//				.println("-----------------------Signals execution----------------------");
//		
//		signalsLib.getSignalsValues();
////		driver.quit();
//	}

}
