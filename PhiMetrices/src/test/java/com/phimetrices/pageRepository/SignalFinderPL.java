package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignalFinderPL {
	
	@FindBy(id="com.android.dialer:id/floating_action_button")
	private WebElement btnDialPad;
	
	@FindBy(id="com.android.dialer:id/star")
	private WebElement btnStar;
	
	@FindBy(id="com.android.dialer:id/pound")
	private WebElement btnHash;
	
	@FindBy(id="com.android.dialer:id/zero")
	private WebElement btnZero;
	
	@FindBy(id="com.android.dialer:id/one")
	private WebElement btnOne;
	
	@FindBy(id="com.android.dialer:id/four")
	private WebElement btnFour;
	
	@FindBy(id="com.android.dialer:id/six")
	private WebElement btnSix;
	
	@FindBy(id="com.android.dialer:id/three")
	private WebElement btnThree;
	
	@FindBy(id="com.android.dialer:id/dialpad_floating_action_button")
	private WebElement btnDial;
	
	@FindBy(name="Phone information")
	private WebElement tabPhoneInformation;
	
	@FindBy(name="Wi-Fi information")
	private WebElement tabWifiInformation;
	
	/**
	 * @return the tabPhoneInformation
	 */
	public WebElement getTabPhoneInformation() {
		return tabPhoneInformation;
	}

	/**
	 * @return the btnDialPad
	 */
	public WebElement getBtnDialPad() {
		return btnDialPad;
	}

	/**
	 * @return the btnStar
	 */
	public WebElement getBtnStar() {
		return btnStar;
	}

	/**
	 * @return the btnHash
	 */
	public WebElement getBtnHash() {
		return btnHash;
	}

	/**
	 * @return the btnZero
	 */
	public WebElement getBtnZero() {
		return btnZero;
	}

	/**
	 * @return the btnOne
	 */
	public WebElement getBtnOne() {
		return btnOne;
	}

	/**
	 * @return the btnFour
	 */
	public WebElement getBtnFour() {
		return btnFour;
	}

	/**
	 * @return the btnSix
	 */
	public WebElement getBtnSix() {
		return btnSix;
	}

	/**
	 * @return the btnThree
	 */
	public WebElement getBtnThree() {
		return btnThree;
	}

	/**
	 * @return the btnDial
	 */
	public WebElement getBtnDial() {
		return btnDial;
	}
	
	
	
}
