package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class InstagramPL {
	
	@FindBy(xpath="//android.widget.FrameLayout[@content-desc='Camera']")
	private WebElement iconCamera;
	
	@FindAll(@FindBy(xpath="//android.support.v7.widget.RecyclerView//android.view.View"))
	private List<WebElement> firstPhoto;
	
	@FindBy(id="com.instagram.android:id/next_button_textview")
	private WebElement tabNext;
	
	@FindBy(xpath="//android.widget.TextView[@text='Share']")
	private WebElement tabNextAgain;
	
	@FindBy(id="com.instagram.android:id/caption_text_view")
	private WebElement txtCaption;
	
	@FindBy(id="com.instagram.android:id/login_username")
	private WebElement txtUsername;
	
	@FindBy(id="com.instagram.android:id/login_password")
	private WebElement txtPassword;
	
	@FindBy(name="Log In")
	private WebElement btnLogin;
	
	@FindBy(xpath="//android.widget.FrameLayout[@content-desc='Profile']")
	private WebElement iconProfile;

	@FindBy(xpath="//android.widget.ImageView[@content-desc='Options']")
	private WebElement tabOption;
	
	@FindBy(name="Log Out")
	private WebElement btnLogOut;
	
	@FindBy(id="com.instagram.android:id/button_positive")
	private WebElement btnConfirmLogout;
	
	@FindBy(id="com.instagram.android:id/row_pending_media_status_textview")
	private WebElement uploadingStatus;
	
	@FindBy(id="com.instagram.android:id/row_feed_photo_people_tagging")
	private WebElement afterUploadStatus;
	
	@FindBy(id="com.instagram.android:id/gallery_folder_menu")
	private WebElement btnGallery;
	
	@FindBy(name="Download")
	private WebElement btnDownload;
	
	
	/**
	 * @return the btnGallery
	 */
	public WebElement getBtnGallery() {
		return btnGallery;
	}

	/**
	 * @return the btnDownload
	 */
	public WebElement getBtnDownload() {
		return btnDownload;
	}

	/**
	 * @return the afterUploadStatus
	 */
	public WebElement getAfterUploadStatus() {
		return afterUploadStatus;
	}

	/**
	 * @return the uploadingStatus
	 */
	public WebElement getUploadingStatus() {
		return uploadingStatus;
	}

	/**
	 * @return the btnConfirmLogout
	 */
	public WebElement getBtnConfirmLogout() {
		return btnConfirmLogout;
	}

	/**
	 * @return the iconProfile
	 */
	public WebElement getIconProfile() {
		return iconProfile;
	}

	/**
	 * @return the tabOption
	 */
	public WebElement getTabOption() {
		return tabOption;
	}

	/**
	 * @return the btnLogOut
	 */
	public WebElement getBtnLogOut() {
		return btnLogOut;
	}
	/**
	 * @return the txtLogin
	 */
	public WebElement getTxtUsername() {
		return txtUsername;
	}

	/**
	 * @return the txtPassword
	 */
	public WebElement getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return the clickBtnLogin
	 */
	public WebElement getBtnLogin() {
		return btnLogin;
	}

	/**
	 * @return the iconCamera
	 */
	public WebElement getIconCamera() {
		return iconCamera;
	}

	/**
	 * @return the firstPhoto
	 */
	public List<WebElement> getFirstPhoto() {
		return firstPhoto;
	}

	/**
	 * @return the tabNext
	 */
	public WebElement getTabNext() {
		return tabNext;
	}

	/**
	 * @return the tabNextAgain
	 */
	public WebElement getTabNextAgain() {
		return tabNextAgain;
	}

	/**
	 * @return the txtCaption
	 */
	public WebElement getTxtCaption() {
		return txtCaption;
	}
	
	
	
	
	
}
