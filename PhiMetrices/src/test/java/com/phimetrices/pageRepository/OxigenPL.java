package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OxigenPL {
	
	@FindBy(id="com.oxigen.oxigenwallet:id/sendButton")
	private WebElement btnSendMoney;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/edtState")
	private WebElement txtAmount;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/selcetFriendAtcmplt")
	private WebElement txtNumber;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/okRechargeButton")
	private WebElement btnSend;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/edtState")
	private WebElement txtPin;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/submit")
	private WebElement btnSubmit;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/okRechargeButton")
	private WebElement btnGreat;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/loadMoneyDash")
	private WebElement btnLoadMoney;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/amount_loadmoney")
	private WebElement txtAmountToLoad;
	
	@FindBy(id="com.oxigen.oxigenwallet:id/text_debitcard_ID")
	private WebElement btnDebitCard;
	
	@FindBy(xpath="//android.widget.EditText[@text='card number']")
	private WebElement txtCardNumber;
	
	@FindBy(xpath="//android.widget.EditText[@text='card holder name']")
	private WebElement txtCardHolderName;
	
	@FindBy(xpath="//android.widget.Spinner[@resource-id='debitexpmnth']")
	private WebElement selectMonth;
	
	@FindBy(xpath="//android.widget.Spinner[@resource-id='debitexpyer']")
	private WebElement selectYear;
	
	@FindBy(xpath="//android.widget.EditText[@text='cvv']")
	private WebElement txtCVV;
	
	@FindBy(xpath="//android.widget.EditText[@text='enter nickname']")
	private WebElement txtNickName;
	
	@FindBy(xpath="//android.view.View[@resource-id='btndebit']")
	private WebElement btnOk;
	
	@FindBy(xpath="//android.view.View[@content-desc='Net Banking']")
	private WebElement tabNetbanking;
	
	@FindBy(id="NBSelectBank")
	private WebElement btnSelectBank;
	
	@FindBy(xpath="//android.view.View[@content-desc='Ok']")
	private WebElement btnOkToLoadMoney;
	
	@FindBy(id="AuthenticationFG.USER_PRINCIPAL")
	private WebElement txtUserID;
	
	@FindBy(id="AuthenticationFG.ACCESS_CODE")
	private WebElement txtPassword;
	
	@FindBy(id="VALIDATE_CREDENTIALS")
	private WebElement btnPaymentLogin;
	
	@FindBy(id="TranRequestManagerFG.GRID_CARD_AUTH_VALUE_1__")
	private WebElement txtCardValue1;
	
	@FindBy(id="TranRequestManagerFG.GRID_CARD_AUTH_VALUE_2__")
	private WebElement txtCardValue2;
	
	@FindBy(id="TranRequestManagerFG.GRID_CARD_AUTH_VALUE_3__")
	private WebElement txtCardValue3;
	
	@FindBy(id="SUBMIT_TRANSACTION")
	private WebElement btnSubmitTransaction;
	
	@FindBy(name="Using the 3D Secure PIN or a One Time Password (OTP)")
	private WebElement btnOTP;
	
	@FindBy(name="Submit")
	private WebElement btnSubmitOTP;
	/**
	 * @return the tabNetbanking
	 */
	public WebElement getTabNetbanking() {
		return tabNetbanking;
	}

	/**
	 * @return the btnSelectBank
	 */
	public WebElement getBtnSelectBank() {
		return btnSelectBank;
	}

	/**
	 * @return the btnOkToLoadMoney
	 */
	public WebElement getBtnOkToLoadMoney() {
		return btnOkToLoadMoney;
	}

	/**
	 * @return the txtUserID
	 */
	public WebElement getTxtUserID() {
		return txtUserID;
	}

	/**
	 * @return the txtPassword
	 */
	public WebElement getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return the btnPaymentLogin
	 */
	public WebElement getBtnPaymentLogin() {
		return btnPaymentLogin;
	}

	/**
	 * @return the txtCardValue1
	 */
	public WebElement getTxtCardValue1() {
		return txtCardValue1;
	}

	/**
	 * @return the txtCardValue2
	 */
	public WebElement getTxtCardValue2() {
		return txtCardValue2;
	}

	/**
	 * @return the txtCardValue3
	 */
	public WebElement getTxtCardValue3() {
		return txtCardValue3;
	}

	/**
	 * @return the btnSubmitTransaction
	 */
	public WebElement getBtnSubmitTransaction() {
		return btnSubmitTransaction;
	}

	/**
	 * @return the btnGreat
	 */
	public WebElement getBtnGreat() {
		return btnGreat;
	}

	/**
	 * @return the btnLoadMoney
	 */
	public WebElement getBtnLoadMoney() {
		return btnLoadMoney;
	}

	/**
	 * @return the txtAmountToLoad
	 */
	public WebElement getTxtAmountToLoad() {
		return txtAmountToLoad;
	}

	/**
	 * @return the btnDebitCard
	 */
	public WebElement getBtnDebitCard() {
		return btnDebitCard;
	}

	/**
	 * @return the txtCardNumber
	 */
	public WebElement getTxtCardNumber() {
		return txtCardNumber;
	}

	/**
	 * @return the txtCardHolderName
	 */
	public WebElement getTxtCardHolderName() {
		return txtCardHolderName;
	}

	/**
	 * @return the selectMonth
	 */
	public WebElement getSelectMonth() {
		return selectMonth;
	}

	/**
	 * @return the selectYear
	 */
	public WebElement getSelectYear() {
		return selectYear;
	}

	/**
	 * @return the txtCVV
	 */
	public WebElement getTxtCVV() {
		return txtCVV;
	}

	/**
	 * @return the txtNickName
	 */
	public WebElement getTxtNickName() {
		return txtNickName;
	}

	/**
	 * @return the btnOk
	 */
	public WebElement getBtnOk() {
		return btnOk;
	}

	/**
	 * @return the btnSendMoney
	 */
	public WebElement getBtnSendMoney() {
		return btnSendMoney;
	}

	/**
	 * @return the txtAmount
	 */
	public WebElement getTxtAmount() {
		return txtAmount;
	}

	/**
	 * @return the txtNumber
	 */
	public WebElement getTxtNumber() {
		return txtNumber;
	}

	/**
	 * @return the btnSend
	 */
	public WebElement getBtnSend() {
		return btnSend;
	}

	/**
	 * @return the txtPin
	 */
	public WebElement getTxtPin() {
		return txtPin;
	}

	/**
	 * @return the btnSubmit
	 */
	public WebElement getBtnSubmit() {
		return btnSubmit;
	}
	
	
	
}
