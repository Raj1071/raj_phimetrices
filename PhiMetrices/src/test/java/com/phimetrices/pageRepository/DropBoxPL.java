package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class DropBoxPL {
	
	@FindBy(id="com.dropbox.android:id/tour_sign_in")
	private WebElement linkSignIn;
	
	@FindAll(@FindBy(xpath="//android.widget.ScrollView"))
	private List<WebElement> txtUsername;
	
	@FindBy(id="com.dropbox.android:id/login_password")
	private WebElement txtPassword;

	@FindBy(id="com.dropbox.android:id/login_submit")
	private WebElement btnSignIn;
	
	@FindBy(id="com.dropbox.android:id/qr_finish_button")
	private WebElement btnNotNow;
	
	@FindBy(id="com.dropbox.android:id/fab_button")
	private WebElement btnAttach;
	
	@FindBy(name="Upload files")
	private WebElement tabUploadFiles;
	
	@FindBy(id="com.android.documentsui:id/menu_sort")
	private WebElement tabMenu;
	
	@FindBy(name="By name")
	private WebElement tabSubMenu;
	
//	@FindBy(xpath="//android.widget.FrameLayout[3]")
//	private WebElement selectedFile;

	@FindBy(name="Downloads")
	private WebElement tabDownloads;
	
	@FindAll(@FindBy(xpath="//android.widget.ListView//android.widget.FrameLayout"))
	private List<WebElement> selectedFile;
	
	@FindBy(xpath="//android.widget.ProgressBar")
	private WebElement processStatus;
	
	@FindBy(id="android:id/button1")
	private WebElement btnReplace;
	
	
	/**
	 * @return the btnReplace
	 */
	public WebElement getBtnReplace() {
		return btnReplace;
	}

	/**
	 * @return the processStatus
	 */
	public WebElement getProcessStatus() {
		return processStatus;
	}

	/**
	 * @return the tabDownloads
	 */
	public WebElement getTabDownloads() {
		return tabDownloads;
	}

	/**
	 * @return the linkSignIn
	 */
	public WebElement getLinkSignIn() {
		return linkSignIn;
	}

	/**
	 * @return the scrollViewOfText
	 */
	public List<WebElement> getTxtUsername() {
		return txtUsername;
	}
	
	/**
	 * @return the txtPassword
	 */
	public WebElement getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return the btnSignIn
	 */
	public WebElement getBtnSignIn() {
		return btnSignIn;
	}

	/**
	 * @return the btnNotNow
	 */
	public WebElement getBtnNotNow() {
		return btnNotNow;
	}

	/**
	 * @return the btnAttach
	 */
	public WebElement getBtnAttach() {
		return btnAttach;
	}

	/**
	 * @return the tabUploadFiles
	 */
	public WebElement getTabUploadFiles() {
		return tabUploadFiles;
	}

	/**
	 * @return the tabMenu
	 */
	public WebElement getTabMenu() {
		return tabMenu;
	}

	/**
	 * @return the tabSubMenu
	 */
	public WebElement getTabSubMenu() {
		return tabSubMenu;
	}

	/**
	 * @return the selectedFile
	 */
	public List<WebElement> getSelectedFile() {
		return selectedFile;
	}
}
