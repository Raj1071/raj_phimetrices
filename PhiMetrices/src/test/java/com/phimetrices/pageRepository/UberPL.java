package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UberPL {
	@FindBy(id="com.ubercab:id/ub__trip_textview_pickup")
	private WebElement tabPickup;
	
	@FindBy(id="com.ubercab:id/ub__trip_view_address_destination")
	private WebElement tabDestination;
	
	@FindBy(id="com.ubercab:id/ub__locationsearch_edittext_search")
	private WebElement txtDestination;
	
	@FindBy(xpath="//android.widget.ListView//android.widget.LinearLayout[1]")
	private WebElement listedDestination;
	
	@FindBy(id="com.ubercab:id/ub__ridepool_toggle_right_item")
	private WebElement btnGetYourOwnRide;
	
	@FindBy(id="com.ubercab:id/ub__request_schedule_schedule_container")
	private WebElement btnCabSchedule;
	
	@FindBy(id="com.ubercab:id/ub_optional__select_date")
	private WebElement btnDateSchedule;
	
	@FindBy(name="29 April 2017")
	private WebElement selectDate;
	
	@FindBy(id="android:id/button1")
	private WebElement btnOkDate;
	
	@FindBy(id="com.ubercab:id/ub_optional__select_time")
	private WebElement btnTimeSchedule;
	
	@FindBy(id="com.ubercab:id/ub__request_button")
	private WebElement btnSetPickupWindow;
	
	@FindBy(id="com.ubercab:id/ub__trip_button_request")
	private WebElement btnScheduleUnderGo;
	
	@FindBy(id="com.ubercab:id/ub__loc_consent_main_view_button_primary")
	private WebElement btnOkForConfirmation;
	
	@FindBy(id="com.ubercab:id/pickup_refinement_done")
	private WebElement btnConfirmationPickUp;

	
	/**
	 * @return the tabPickup
	 */
	public WebElement getTabPickup() {
		return tabPickup;
	}

	/**
	 * @return the btnGetYourOwnRide
	 */
	public WebElement getBtnGetYourOwnRide() {
		return btnGetYourOwnRide;
	}

	/**
	 * @return the tabDestination
	 */
	public WebElement getTabDestination() {
		return tabDestination;
	}

	/**
	 * @return the txtDestination
	 */
	public WebElement getTxtDestination() {
		return txtDestination;
	}

	/**
	 * @return the listedDestination
	 */
	public WebElement getListedDestination() {
		return listedDestination;
	}

	/**
	 * @return the btnCabSchedule
	 */
	public WebElement getBtnCabSchedule() {
		return btnCabSchedule;
	}

	/**
	 * @return the btnDateSchedule
	 */
	public WebElement getBtnDateSchedule() {
		return btnDateSchedule;
	}

	/**
	 * @return the selectDate
	 */
	public WebElement getSelectDate() {
		return selectDate;
	}

	/**
	 * @return the btnOkDate
	 */
	public WebElement getBtnOkDate() {
		return btnOkDate;
	}

	/**
	 * @return the btnTimeSchedule
	 */
	public WebElement getBtnTimeSchedule() {
		return btnTimeSchedule;
	}

	/**
	 * @return the btnSetPickupWindow
	 */
	public WebElement getBtnSetPickupWindow() {
		return btnSetPickupWindow;
	}

	/**
	 * @return the btnScheduleUnderGo
	 */
	public WebElement getBtnScheduleUnderGo() {
		return btnScheduleUnderGo;
	}

	/**
	 * @return the btnOkForConfirmation
	 */
	public WebElement getBtnOkForConfirmation() {
		return btnOkForConfirmation;
	}

	/**
	 * @return the btnConfirmationPickUp
	 */
	public WebElement getBtnConfirmationPickUp() {
		return btnConfirmationPickUp;
	}
	
	
}
