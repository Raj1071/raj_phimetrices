package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MobileLocationFinderPL {
	
	@FindBy(id="com.AndLocation.AndLocation:id/lblposLat")
	private WebElement latValue;
	
	@FindBy(id="com.AndLocation.AndLocation:id/lblposLong")
	private WebElement longValue;

	/**
	 * @return the latValue
	 */
	public WebElement getLatValue() {
		return latValue;
	}

	/**
	 * @return the longValue
	 */
	public WebElement getLongValue() {
		return longValue;
	}
	
	
}
