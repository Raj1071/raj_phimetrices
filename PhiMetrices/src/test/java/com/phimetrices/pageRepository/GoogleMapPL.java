package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleMapPL {
	@FindBy(xpath="//android.widget.TextView[@text='GO']")
	private WebElement btnGo;
	
	@FindBy(id="com.google.android.apps.maps:id/directions_endpoint_textbox")
	private WebElement tabChooseDestination;
	
	@FindBy(id="com.google.android.apps.maps:id/search_omnibox_edit_text")
	private WebElement txtChooseDestination;
	
	@FindBy(xpath="//android.widget.LinearLayout//android.widget.RelativeLayout[1]")
	private WebElement tabListItem;
	
	@FindBy(className="android.widget.ProgressBar")
	private WebElement processBar;
	
	

	/**
	 * @return the processBar
	 */
	public WebElement getProcessBar() {
		return processBar;
	}

	/**
	 * @return the btnGo
	 */
	public WebElement getBtnGo() {
		return btnGo;
	}

	/**
	 * @return the tabChooseDestination
	 */
	public WebElement getTabChooseDestination() {
		return tabChooseDestination;
	}

	/**
	 * @return the txtChooseDestination
	 */
	public WebElement getTxtChooseDestination() {
		return txtChooseDestination;
	}

	/**
	 * @return the tabListItem
	 */
	public WebElement getTabListItem() {
		return tabListItem;
	}
	
	
}
