package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class FaceBookPL {

	@FindBy(id = "com.facebook.katana:id/login_username")
	private WebElement txtUserName;

	@FindBy(id = "com.facebook.katana:id/login_password")
	private WebElement txtPassword;

	@FindBy(id = "com.facebook.katana:id/login_login")
	private WebElement btnLogin;

	@FindBy(id = "com.facebook.katana:id/dbl_off")
	private WebElement btnNotNow;

	@FindBy(id = "//android.support.v7.widget.RecyclerView//android.view.ViewGroup[@text='Hi, Helen. Tell your friends about your day']")
	private WebElement areaStatus;
	
	@FindAll(@FindBy(xpath = "//android.widget.ListView//com.facebook.fbui.widget.contentview.ContentView"))
	private List<WebElement> tabViewProfile;
	
	@FindBy(id = "com.facebook.katana:id/feed_composer_photo_button")
	private WebElement tabPhotos;
	
	@FindBy(name = "Photo/Video")
	private WebElement tabPhotosVideos;

	@FindBy(id = "com.facebook.katana:id/composer_status_text")
	private WebElement txtStatus;

	@FindBy(id = "com.facebook.katana:id/primary_named_button")
	private WebElement btnPost;

	@FindBy(id = "com.facebook.katana:id/progress_horizontal")
	private WebElement loader;

	@FindBy(id = "com.facebook.katana:id/feed_composer_photo_button")
	private WebElement linkPhoto;

	@FindAll(@FindBy(xpath = "//android.support.v7.widget.RecyclerView//android.widget.ImageView"))
	private List<WebElement> firstPhoto;

	@FindBy(id = "com.facebook.katana:id/primary_named_button")
	private WebElement btnDone;

	@FindBy(id = "com.facebook.katana:id/bookmarks_tab")
	private WebElement clickTabBookmark;

	@FindBy(xpath = "//com.facebook.fbui.widget.contentview.ContentView[@content-desc='Log Out']")
	private WebElement clickLogout;

	@FindBy(xpath = "//android.support.v4.view.ViewPager")
	private WebElement listContent;
	
	@FindBy(name = "Story Menu")
	private WebElement statusPost;
	
	@FindAll(@FindBy(id = "com.facebook.katana:id/video_grid_icon"))
	private List<WebElement> tabVideo;
	
	@FindBy(xpath = "//android.view.ViewGroup[@text='We couldn't post this. Tap for more info.']")
	private WebElement uploadError;
	
	@FindBy(id = "com.facebook.katana:id/offline_retry_header")
	private WebElement imgUploadError;
	
	@FindBy(id = "com.facebook.katana:id/search_edit_text")
	private WebElement validateFB;
	
	
	public List<WebElement> getTabViewProfile() {
		return tabViewProfile;
	}

	public WebElement getTabPhotos() {
		return tabPhotos;
	}

	/**
	 * @return the validateFB
	 */
	public WebElement getValidateFB() {
		return validateFB;
	}

	/**
	 * @return the imgUploadError
	 */
	public WebElement getImgUploadError() {
		return imgUploadError;
	}

	/**
	 * @return the uploadError
	 */
	public WebElement getUploadError() {
		return uploadError;
	}

	/**
	 * @return the tabVideo
	 */
	public List<WebElement> getTabVideo() {
		return tabVideo;
	}

	/**
	 * @return the statusPost
	 */
	public WebElement getStatusPost() {
		return statusPost;
	}

	/**
	 * @return the tabPhotosVideos
	 */
	public WebElement getTabPhotosVideos() {
		return tabPhotosVideos;
	}

	/**
	 * @return the listContent
	 */
	public WebElement getListContent() {
		return listContent;
	}

	/**
	 * @return the clickTabBookmark
	 */
	public WebElement getClickTabBookmark() {
		return clickTabBookmark;
	}

	/**
	 * @return the clickLogout
	 */
	public WebElement getClickLogout() {
		return clickLogout;
	}

	public WebElement getTxtUserName() {
		return txtUserName;
	}

	public WebElement getTxtPassword() {
		return txtPassword;
	}

	public WebElement getBtnLogin() {
		return btnLogin;
	}

	public WebElement getBtnNotNow() {
		return btnNotNow;
	}

	public WebElement getAreaStatus() {
		return areaStatus;
	}

	public WebElement getTxtStatus() {
		return txtStatus;
	}

	public WebElement getBtnPost() {
		return btnPost;
	}

	public WebElement getLoader() {
		return loader;
	}

	public WebElement getLinkPhoto() {
		return linkPhoto;
	}

	public List<WebElement> getFirstPhoto() {
		return firstPhoto;
	}

	public WebElement getBtnDone() {
		return btnDone;
	}

}
