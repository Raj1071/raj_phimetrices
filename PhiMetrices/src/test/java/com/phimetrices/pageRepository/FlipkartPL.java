package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FlipkartPL {

	@FindBy(id="com.flipkart.android:id/search_widget_textbox")
	private WebElement searchBox;
	
	@FindBy(id="com.flipkart.android:id/search_autoCompleteTextView")
	private WebElement txtSearch;
	
	@FindBy(xpath="//android.view.ViewGroup[3]//android.widget.LinearLayout[@content-desc='product container']")
	private WebElement tabItem;
	
	public WebElement getSearchBox() {
		return searchBox;
	}

	/**
	 * @return the txtSearch
	 */
	public WebElement getTxtSearch() {
		return txtSearch;
	}

	/**
	 * @return the tabItem
	 */
	public WebElement getTabItem() {
		return tabItem;
	}
	
//	@FindBy(name="Disney Frozen Character Art Plastic Pencil Box")
//	private WebElement tabItem;
	
	
	
}
