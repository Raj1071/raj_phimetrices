package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AmazonPL {
	
	@FindBy(id="in.amazon.mShop.android.shopping:id/web_home_shop_by_department_label")
	private WebElement btnShopBycategory;
	
	@FindBy(id="in.amazon.mShop.android.shopping:id/rs_search_src_text")
	private WebElement tabSearchItem;
	
	@FindBy(name="Sensodyne Sensitive Toothbrush (2+1 Pack)")
	private WebElement pickItem;
	
	
	

	/**
	 * @return the tabSearchItem
	 */
	public WebElement getTabSearchItem() {
		return tabSearchItem;
	}



	/**
	 * @return the btnShopBycategory
	 */
	public WebElement getBtnShopBycategory() {
		return btnShopBycategory;
	}
	
	
}
