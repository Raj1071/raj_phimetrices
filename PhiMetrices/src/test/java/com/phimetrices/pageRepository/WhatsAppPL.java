package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class WhatsAppPL {

		@FindBy(name="Search")
		private WebElement iconSearch;
		
		@FindBy(name="Search�")
		private WebElement txtSearch;
		
		@FindBy(id="com.whatsapp:id/contact_row_container")
		private WebElement selectUser;
		
		@FindBy(name="Attach")
		private WebElement clickAttach;
		
		@FindBy(name="Gallery")
		private WebElement clickGallery;
		
		@FindBy(name="Photos")
		private WebElement clickPhotosTab;
		
		/**
		 * @return the clickPhotosTab
		 */
		public WebElement getClickPhotosTab() {
			return clickPhotosTab;
		}

		@FindBy(name="Camera photos")
		private WebElement clickCameraPhotos;
		
		@FindAll(@FindBy(xpath="//android.support.v7.widget.RecyclerView//android.view.View"))
		private List<WebElement> firstPhoto;
		
		@FindBy(id="com.whatsapp:id/send")
		private WebElement clickSendButtonOfImage;
		
		@FindBy(name="Videos")
		private WebElement clickVideosTab;
		
		@FindBy(name="All videos")
		private WebElement clickAllVideos;
		
		@FindAll(@FindBy(xpath="//android.support.v7.widget.RecyclerView//android.view.View"))
		private List<WebElement> firstVideo;
		
		@FindBy(name="Send")
		private WebElement clickSendButtonOfVideos;
		
		@FindBy(name="Download")
		private WebElement downloadVideo;
		
		@FindBy(id="com.whatsapp:id/control_btn")
		private WebElement downloadImage;
		
		@FindBy(name="Navigate up")
		private WebElement clickNavigateBack;
		
		@FindBy(id="com.whatsapp:id/control_frame")
		private WebElement lockDisplayed;
		
		@FindBy(id="com.whatsapp:id/progress_bar")
		private WebElement lockVideoDisplayed;
		
		@FindBy(id="com.whatsapp:id/progress_bar")
		private WebElement uploadProcessBar;
		
		/**
		 * @return the uploadProcessBar
		 */
		public WebElement getUploadProcessBar() {
			return uploadProcessBar;
		}

		/**
		 * @return the lockVideoDisplayed
		 */
		public WebElement getLockVideoDisplayed() {
			return lockVideoDisplayed;
		}

		/**
		 * @return the lockDisplayed
		 */
		public WebElement getLockDisplayed() {
			return lockDisplayed;
		}

		/**
		 * @return the clickNavigateBack
		 */
		public WebElement getClickNavigateBack() {
			return clickNavigateBack;
		}

		/**
		 * @return the firstMedia
		 */
		public WebElement getDownloadImage() {
			return downloadImage;
		}

		/**
		 * @return the listImage
		 */
		public WebElement getDownloadVideo() {
			return downloadVideo;
		}

		/**
		 * @return the iconSearch
		 */
		public WebElement getIconSearch() {
			return iconSearch;
		}

		/**
		 * @return the txtSearch
		 */
		public WebElement getTxtSearch() {
			return txtSearch;
		}

		/**
		 * @return the selectUser
		 */
		public WebElement getSelectUser() {
			return selectUser;
		}

		/**
		 * @return the clickAttach
		 */
		public WebElement getClickAttach() {
			return clickAttach;
		}

		/**
		 * @return the clickGallery
		 */
		public WebElement getClickGallery() {
			return clickGallery;
		}

		/**
		 * @return the clickCameraPhotos
		 */
		public WebElement getClickCameraPhotos() {
			return clickCameraPhotos;
		}

		/**
		 * @return the click3rdCellImage
		 */
		public List<WebElement> getFirstPhoto() {
			return firstPhoto;
		}

		/**
		 * @return the clickSendButtonOfImage
		 */
		public WebElement getClickSendButtonOfImage() {
			return clickSendButtonOfImage;
		}

		/**
		 * @return the clickVideosTab
		 */
		public WebElement getClickVideosTab() {
			return clickVideosTab;
		}

		/**
		 * @return the clickAllVideos
		 */
		public WebElement getClickAllVideos() {
			return clickAllVideos;
		}

		/**
		 * @return the click1stCellVideo
		 */
		public  List<WebElement> getFirstVideo(){
			return firstVideo;
		}

		/**
		 * @return the clickSendButtonOfVideos
		 */
		public WebElement getClickSendButtonOfVideos() {
			return clickSendButtonOfVideos;
		}
		
		
		
}
