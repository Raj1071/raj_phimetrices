package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OlaPL {
	@FindBy(xpath="//android.widget.TextView[@text='Micro']")
	private WebElement btnMicroCar;
	
	@FindBy(id="com.olacabs.customer:id/textView_drop_location")
	private WebElement tabDropLocation;
	
	@FindBy(id="com.olacabs.customer:id/searchEdit")
	private WebElement txtDropLocation;
	
	@FindBy(id="com.olacabs.customer:id/pick_up_location_parent")
	private WebElement tabFirstList;
	
	@FindBy(id="com.olacabs.customer:id/button_ride_now")
	private WebElement btnRideNow;
	
	@FindBy(id="com.olacabs.customer:id/button_ride_confirm")
	private WebElement btnConfirmBooking;

	/**
	 * @return the btnMicroCar
	 */
	public WebElement getBtnMicroCar() {
		return btnMicroCar;
	}

	/**
	 * @return the tabDropLocation
	 */
	public WebElement getTabDropLocation() {
		return tabDropLocation;
	}

	/**
	 * @return the txtDropLocation
	 */
	public WebElement getTxtDropLocation() {
		return txtDropLocation;
	}

	/**
	 * @return the tabFirstList
	 */
	public WebElement getTabFirstList() {
		return tabFirstList;
	}

	/**
	 * @return the btnRideNow
	 */
	public WebElement getBtnRideNow() {
		return btnRideNow;
	}

	/**
	 * @return the btnConfirmBooking
	 */
	public WebElement getBtnConfirmBooking() {
		return btnConfirmBooking;
	}
	
	
}
