package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class TwitterPL {
	
	@FindBy(id="com.twitter.android:id/log_in")
	private WebElement btnSignIn;
	
	@FindBy(id="com.twitter.android:id/login_identifier")
	private WebElement txtUsername;
	
	@FindBy(id="com.twitter.android:id/login_password")
	private WebElement txtPassword;
	
	@FindBy(id="com.twitter.android:id/login_login")
	private WebElement btnLogin;
	
	@FindBy(id="com.twitter.android:id/composer_write")
	private WebElement btnCompose;
	
	@FindBy(id="com.twitter.android:id/gallery")
	private WebElement btnGallery;
	
	@FindAll(@FindBy(xpath = "//android.widget.FrameLayout//android.widget.ImageView"))
	private List<WebElement> firstPhoto;
	
	@FindBy(xpath="//android.widget.ImageView[@content-desc='Image']")
	private WebElement listGallery;
	
	@FindBy(id="com.twitter.android:id/tweet_text")
	private WebElement txtStatus;
	
	@FindBy(name="Tweet")
	private WebElement btnTweet;
	
	@FindBy(id="com.twitter.android:id/drawer_icon")
	private WebElement tabNavigateToLogout;
	
	@FindBy(name="Settings and privacy")
	private WebElement btnSettingAndPrivacy;
	
	@FindBy(name="Log out")
	private WebElement btnLogout;
	
	@FindBy(id="android:id/button1")
	private WebElement btnLogoutOK;
	
	@FindBy(name="Raj @Raj_1071. . . . happening now. .")
	private WebElement uploadStatus;
	
	@FindBy(name="Home Tab")
	private WebElement tabHome;
	
	@FindBy(id="com.twitter.android:id/gallery_name")
	private WebElement tabGallery;
	
	@FindBy(name="More...")
	private WebElement tabWhatsappImages;
	
	
	
	public List<WebElement> getFirstPhoto() {
		return firstPhoto;
	}

	/**
	 * @return the tabGallery
	 */
	public WebElement getTabGallery() {
		return tabGallery;
	}

	/**
	 * @return the tabWhatsappImages
	 */
	public WebElement getTabWhatsappImages() {
		return tabWhatsappImages;
	}

	/**
	 * @return the btnLogoutOK
	 */
	public WebElement getBtnLogoutOK() {
		return btnLogoutOK;
	}

	/**
	 * @return the uploadStatus
	 */
	public WebElement getUploadStatus() {
		return uploadStatus;
	}

	/**
	 * @return the tabHome
	 */
	public WebElement getTabHome() {
		return tabHome;
	}

	/**
	 * @return the btnSignIn
	 */
	public WebElement getBtnSignIn() {
		return btnSignIn;
	}

	/**
	 * @return the txtUsername
	 */
	public WebElement getTxtUsername() {
		return txtUsername;
	}

	/**
	 * @return the txtPassword
	 */
	public WebElement getTxtPassword() {
		return txtPassword;
	}

	/**
	 * @return the btnLogin
	 */
	public WebElement getBtnLogin() {
		return btnLogin;
	}

	/**
	 * @return the btnCompose
	 */
	public WebElement getBtnCompose() {
		return btnCompose;
	}

	/**
	 * @return the btnGallery
	 */
	public WebElement getBtnGallery() {
		return btnGallery;
	}

	/**
	 * @return the listGallery
	 */
	public WebElement getListGallery() {
		return listGallery;
	}

	/**
	 * @return the txtStatus
	 */
	public WebElement getTxtStatus() {
		return txtStatus;
	}

	/**
	 * @return the btnTweet
	 */
	public WebElement getBtnTweet() {
		return btnTweet;
	}

	/**
	 * @return the tabNavigateToLogout
	 */
	public WebElement getTabNavigateToLogout() {
		return tabNavigateToLogout;
	}

	/**
	 * @return the btnSettingAndPrivacy
	 */
	public WebElement getBtnSettingAndPrivacy() {
		return btnSettingAndPrivacy;
	}

	/**
	 * @return the btnLogout
	 */
	public WebElement getBtnLogout() {
		return btnLogout;
	}

	
	
}
