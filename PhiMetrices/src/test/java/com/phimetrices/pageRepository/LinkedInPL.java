package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class LinkedInPL {

	@FindBy(name = "Sign in")
	private WebElement clickSignIn;
	
	@FindBy(id = "com.google.android.gms:id/cancel")
	private WebElement tabNoneOfTheAboveMail;

	@FindBy(id = "com.linkedin.android:id/growth_login_join_fragment_email_address")
	private WebElement txtEmailId;

	@FindBy(id = "com.linkedin.android:id/growth_login_join_fragment_password")
	private WebElement txtPassword;

	@FindBy(id = "com.linkedin.android:id/growth_login_fragment_sign_in")
	private WebElement clickBtnSignIn;

	@FindBy(name = "Share button")
	private WebElement clickIconShare;

	@FindBy(id = "com.linkedin.android:id/sharing_compose_attach_image_button")
	private WebElement btnCamera;

	@FindBy(name = "Choose a photo from the gallery")
	private WebElement btnChoosePhoto;
	
	@FindAll(@FindBy(xpath = "//android.widget.LinearLayout//android.widget.FrameLayout[1]"))
	private List <WebElement> selectImage;
	
//	@FindAll(@FindBy(xpath = "//android.widget.LinearLayout//android.widget.FrameLayout(0)"))
//	private List <WebElement> selectImage2;

	@FindBy(id = "com.linkedin.android:id/sharing_compose_text_input")
	private WebElement txtAboutPost;

	@FindBy(id = "com.linkedin.android:id/sharing_compose_post_button")
	private WebElement clickButtonPost;

	@FindBy(name = "Close")
	private WebElement clickTabClose;

	@FindBy(id = "com.linkedin.android:id/me_launcher")
	private WebElement btnProfileLauncher;

	@FindBy(id = "com.linkedin.android:id/profile_toolbar_settings_button")
	private WebElement btnSetting;
	
	@FindBy(name = "Communications")
	private WebElement btnCommunications;
	
	@FindBy(name = "Sign out")
	private WebElement btnSignOut;

	@FindBy(xpath = "//android.widget.LinearLayout//*[5]//android.widget.ImageView[@resource-id='com.linkedin.android:id/home_nav_tab_icon']")
	private WebElement btnProfileLauncherNxt;
	
	@FindBy(id = "com.linkedin.android:id/profile_view_top_card_settings_button")
	private WebElement btnSettingNxt;
	
	@FindBy(id = "com.linkedin.android:id/feed_component_progress_bar")
	private WebElement uploadStatus;
	
	@FindBy(name = "Feed, Tab 1 of 5")
	private WebElement tabHome;
	
	
//	public List<WebElement> getSelectImage2() {
//		return selectImage2;
//	}

	/**
	 * @return the uploadStatus
	 */
	public WebElement getUploadStatus() {
		return uploadStatus;
	}

	/**
	 * @return the tabHome
	 */
	public WebElement getTabHome() {
		return tabHome;
	}

	/**
	 * @return the tabNoneOfTheAboveMail
	 */
	public WebElement getTabNoneOfTheAboveMail() {
		return tabNoneOfTheAboveMail;
	}

	public WebElement getBtnProfileLauncherNxt() {
		return btnProfileLauncherNxt;
	}

	/**
	 * @return the btnSettingNxt
	 */
	public WebElement getBtnSettingNxt() {
		return btnSettingNxt;
	}

	/**
	 * @return the btnCommunications
	 */
	public WebElement getBtnCommunications() {
		return btnCommunications;
	}
	
	public WebElement getBtnProfileLauncher() {
		return btnProfileLauncher;
	}
	
	public WebElement getBtnSetting() {
		return btnSetting;
	}

	public WebElement getBtnSignOut() {
		return btnSignOut;
	}

	public WebElement getClickTabClose() {
		return clickTabClose;
	}

	public WebElement getClickSignIn() {
		return clickSignIn;
	}

	public WebElement getTxtEmailId() {
		return txtEmailId;
	}

	public WebElement getTxtPassword() {
		return txtPassword;
	}

	public WebElement getClickBtnSignIn() {
		return clickBtnSignIn;
	}

	public WebElement getClickIconShare() {
		return clickIconShare;
	}


	public WebElement getBtnCamera() {
		return btnCamera;
	}

	public WebElement getBtnChoosePhoto() {
		return btnChoosePhoto;
	}

	public List<WebElement> getSelectImage() {
		return selectImage;
	}

	public WebElement getTxtAboutPost() {
		return txtAboutPost;
	}

	public WebElement getClickButtonPost() {
		return clickButtonPost;
	}

}
