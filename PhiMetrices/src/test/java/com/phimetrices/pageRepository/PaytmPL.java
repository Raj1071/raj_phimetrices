package com.phimetrices.pageRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class PaytmPL {
	
	@FindBy(xpath = "//android.widget.TextView[@text='Pay']")
	private WebElement btnPay;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Mobile Number']")
	private WebElement tabMobileNumber;
	
	@FindBy(id = "net.one97.paytm:id/txt_p2p_account")
	private WebElement txtMobileNumber;
	
	@FindBy(id = "net.one97.paytm:id/edit_p2p_amount")
	private WebElement txtAmount;
	
	@FindBy(id = "net.one97.paytm:id/edit_p2p_comment")
	private WebElement txtDescription;
	
	@FindBy(id = "net.one97.paytm:id/btn_p2p_send_money")
	private WebElement btnProceed;
	
	@FindBy(id = "net.one97.paytm:id/w_custom_dialog_btn_positive")
	private WebElement btnConfirmProceed;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Add Money']")
	private WebElement tabAddMoney;
	
	@FindBy(id = "net.one97.paytm:id/text_amount")
	private WebElement txtAddAmount;
	
	@FindBy(id = "net.one97.paytm:id/btn_add_to_wallet")
	private WebElement btnAddMoney;
	
	@FindBy(xpath = "//android.widget.EditText[@resource-id='card-number']")
	private WebElement txtCardNumber;
	
	@FindBy(xpath = "//android.widget.Spinner[@resource-id='dcMonth']")
	private WebElement dropDownMonth;
	
	@FindAll(@FindBy(xpath = "//android.widget.ListView/android.widget.CheckedTextView"))
	private List<WebElement> listMonth;

	@FindBy(xpath = "//android.widget.Spinner[@resource-id='dcYear']")
	private WebElement dropDownYear;
	
	@FindAll(@FindBy(xpath = "//android.widget.ListView"))
	private List<WebElement> listYear;
	
	@FindBy(xpath = "//android.view.View[@content-desc='CVV']/following-sibling::android.widget.EditText")
	private WebElement txtCVVNumber;
	
	@FindBy(xpath = "//android.widget.Button[@content-desc='Pay now']")
	private WebElement btnPayNow;
	
	@FindBy(id = "net.one97.paytm:id/img_tab_bar_paytm")
	private WebElement btnHome;
	
	@FindBy(id = "net.one97.paytm:id/iv_close_icon")
	private WebElement btnClose;
	
	

	/**
	 * @return the btnClose
	 */
	public WebElement getBtnClose() {
		return btnClose;
	}

	/**
	 * @return the listMonth
	 */
	public List<WebElement> getListMonth() {
		return listMonth;
	}

	/**
	 * @return the listYear
	 */
	public List<WebElement> getListYear() {
		return listYear;
	}

	/**
	 * @return the btnHome
	 */
	public WebElement getBtnHome() {
		return btnHome;
	}

	public WebElement getTabAddMoney() {
		return tabAddMoney;
	}

	public WebElement getTxtAddAmount() {
		return txtAddAmount;
	}

	public WebElement getBtnAddMoney() {
		return btnAddMoney;
	}


	public WebElement getTxtCardNumber() {
		return txtCardNumber;
	}

	
	public WebElement getDropDownMonth() {
		return dropDownMonth;
	}

	
	public WebElement getDropDownYear() {
		return dropDownYear;
	}

	/**
	 * @return the txtCVVNumber
	 */
	public WebElement getTxtCVVNumber() {
		return txtCVVNumber;
	}

	/**
	 * @return the btnPayNow
	 */
	public WebElement getBtnPayNow() {
		return btnPayNow;
	}
	/**
	 * @return the btnPay
	 */
	public WebElement getBtnPay() {
		return btnPay;
	}

	/**
	 * @return the tabMobileNumber
	 */
	public WebElement getTabMobileNumber() {
		return tabMobileNumber;
	}

	/**
	 * @return the txtMobileNumber
	 */
	public WebElement getTxtMobileNumber() {
		return txtMobileNumber;
	}

	/**
	 * @return the txtAmount
	 */
	public WebElement getTxtAmount() {
		return txtAmount;
	}

	/**
	 * @return the txtDescription
	 */
	public WebElement getTxtDescription() {
		return txtDescription;
	}

	/**
	 * @return the btnProceed
	 */
	public WebElement getBtnProceed() {
		return btnProceed;
	}

	/**
	 * @return the btnConfirmProceed
	 */
	public WebElement getBtnConfirmProceed() {
		return btnConfirmProceed;
	}
	
	
	
}
