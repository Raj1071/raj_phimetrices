package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendingMessagePL {
	
	@FindBy(id="com.android.mms:id/action_compose_new")
	private WebElement btnComposeSMS;
	
	@FindBy(id="com.android.mms:id/recipients_editor")
	private WebElement txtnumber;
	
	@FindBy(id="com.android.mms:id/embedded_text_editor")
	private WebElement txtComposeMessage;
	
	@FindBy(id="com.android.mms:id/send_button_sms")
	private WebElement btnSendMessage;

	/**
	 * @return the btnComposeSMS
	 */
	public WebElement getBtnComposeSMS() {
		return btnComposeSMS;
	}

	/**
	 * @return the txtnumber
	 */
	public WebElement getTxtnumber() {
		return txtnumber;
	}

	/**
	 * @return the txtComposeMessage
	 */
	public WebElement getTxtComposeMessage() {
		return txtComposeMessage;
	}

	/**
	 * @return the btnSendMessage
	 */
	public WebElement getBtnSendMessage() {
		return btnSendMessage;
	}
	
	
}
