package com.phimetrices.pageRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LocationAppPL {

	@FindBy(id = "com.demo.locationtest:id/locationTxt")
	private WebElement txtLatLongValue;
	
	@FindBy(id = "com.demo.locationtest:id/cellSigTxt")
	private WebElement txtSignalValue;
	
	@FindBy(id = "com.demo.locationtest:id/refresh")
	private WebElement btnRefresh;

	public WebElement getTxtLatLongValue() {
		return txtLatLongValue;
	}

	public WebElement getTxtSignalValue() {
		return txtSignalValue;
	}

	public WebElement getBtnRefresh() {
		return btnRefresh;
	}
	
	

}
