package com.phimetrices.driver;

import java.util.HashMap;
import java.util.Map;

import utils.load.dataSource.ExcelLib;

public class DeviceBean {

	String appiumPort, ipAddress, devicePort, bootStrapPort, platformName,
			androidVersion, deviceName, sheetName,simCard,udid;
	ExcelLib excelData;
	public static Map<String, DeviceBean> deviceInfo;
	int rowCount, i;

	public DeviceBean(String appiumPort, String ipAddress, String devicePort,
			String bootStrapPort, String platformName, String androidVersion,
			String deviceName,String simCard,String udid)
			throws Exception {

		this.appiumPort = appiumPort;
		this.ipAddress = ipAddress;
		this.devicePort = devicePort;
		this.bootStrapPort = bootStrapPort;
		this.platformName = platformName;
		this.androidVersion = androidVersion;
		this.deviceName = deviceName;
		this.simCard=simCard;
		this.udid=udid;

	}
	
	public DeviceBean() {
	}

	public String getSimCard() {
		return simCard;
	}


	public void setSimCard(String simCard) {
		this.simCard = simCard;
	}

	public String getAppiumPort() {
		return appiumPort;
	}

	public void setAppiumPort(String appiumPort) {
		this.appiumPort = appiumPort;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDevicePort() {
		return devicePort;
	}

	public void setDevicePort(String devicePort) {
		this.devicePort = devicePort;
	}

	public String getBootStrapPort() {
		return bootStrapPort;
	}

	public void setBootStrapPort(String bootStrapPort) {
		this.bootStrapPort = bootStrapPort;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	
	public String getUdid() {
		return udid;
	}

	
	public void setUdid(String udid) {
		this.udid = udid;
	}

	public void getData() throws Exception {
		deviceInfo = new HashMap<String, DeviceBean>();
		sheetName = "DeviceInfo";
		excelData = new ExcelLib();
		rowCount = excelData.getRowCount(sheetName,"DeviceInfo.xls");
		System.out.println("rowCount=" + rowCount);
		excelData = new ExcelLib();
		for (i = 1; i < rowCount; i++) {
			appiumPort = excelData.getExcelData(sheetName, i, 1,"DeviceInfo.xls");
			System.out.println(appiumPort);
			ipAddress = excelData.getExcelData(sheetName, i, 2,"DeviceInfo.xls");
			devicePort = excelData.getExcelData(sheetName, i, 3,"DeviceInfo.xls");
			bootStrapPort = excelData.getExcelData(sheetName, i, 4,"DeviceInfo.xls");
			System.out.println("appiumPort " + appiumPort);
			System.out.println("devicePort " + devicePort);
			System.out.println("bootStrapPort " + bootStrapPort);
			platformName = excelData.getExcelData(sheetName, i, 5,"DeviceInfo.xls");
			androidVersion = excelData.getExcelData(sheetName, i, 6,"DeviceInfo.xls");
			deviceName = excelData.getExcelData(sheetName, i, 7,"DeviceInfo.xls");
			simCard = excelData.getExcelData(sheetName, i, 9,"DeviceInfo.xls");
			udid=excelData.getExcelData(sheetName, i, 8,"DeviceInfo.xls");
			System.out.println("---------simCard-----"+simCard);
			deviceInfo.put(deviceName, new DeviceBean(appiumPort, ipAddress,
					devicePort, bootStrapPort, platformName, androidVersion,
					deviceName,simCard,udid));
		}
		
		System.out.println("---------device Info value--------"+deviceInfo);
	}
}
