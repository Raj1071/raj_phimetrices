package com.phimetrices.driver;

public class MultiThreadExample extends Thread{

	int value;
	MultiThreadExample(int value){
		this.value=value;
	}
	
	  private static int count = 0;

	    public void incrementCount() {
	        synchronized (MultiThreadExample.class) {
	            count++;
	            System.out.println(count);
	            System.out.println("This is currently running on a separate thread, " +  
	    	            "the id is: " + Thread.currentThread().getId());  
	        }
	    }
	public static void main(String[] args) throws Exception {
		
		MultiThreadExample m1 = new MultiThreadExample(count);
		MultiThreadExample m2 = new MultiThreadExample(count);
		
		
		 Thread thread = new Thread(m1);  
		 Thread thread1 = new Thread(m2);
		    thread.start();
		    
		    thread1.start();
	}
	
	 @Override  
	  public void run()   
	  {  
	    incrementCount();
	  }  
	 
}
