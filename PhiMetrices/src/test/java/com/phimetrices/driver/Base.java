package com.phimetrices.driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import utils.load.dataSource.ExcelLib;

public class Base {
	ExcelLib excelData;
	AppiumServiceBuilder appiumService;
	public static AndroidDriver<WebElement> driver = null;
	int rowCount, i, threadId;
	static String appiumPort, ipAddress, devicePort, bootStrapPort, sheetName,
			platformName, androidVersion, deviceName, appiumServiceUrl,
			appPackage, activityName, simCard;
	
	public static void main(String ags[]) throws Exception {
		ExcelLib excelData = new ExcelLib();
		String sheetName = "DeviceInfo";
		int rowCount = excelData.getRowCount(sheetName, "DeviceInfo.xls");
		List<AppiumServiceBuilder> appiumServiceList = new ArrayList<AppiumServiceBuilder>(
				rowCount);

		DeviceBean db = new DeviceBean();
		db.getData();
		for (Map.Entry<String, DeviceBean> entry : DeviceBean.deviceInfo
				.entrySet()) {
			appiumPort = entry.getValue().getAppiumPort();
			ipAddress = entry.getValue().getIpAddress();
			devicePort = entry.getValue().getDevicePort();
			bootStrapPort = entry.getValue().getBootStrapPort();
			platformName = entry.getValue().getPlatformName();
			androidVersion = entry.getValue().getAndroidVersion();
			deviceName = entry.getValue().getDeviceName();
			simCard = entry.getValue().getSimCard();

			appiumServiceList
					.add(new AppiumServiceBuilder()
							.usingDriverExecutable(
									new File(
											"C:\\Program Files (x86)\\Appium\\node.exe"))
							.withAppiumJS(
									new File(
											"C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\appium.js"))
							.withIPAddress("127.0.0.1")
							.withArgument(
									AndroidServerFlag.BOOTSTRAP_PORT_NUMBER,
									Integer.toString(Integer
											.parseInt(bootStrapPort)))
							.withLogFile(
									new File("src/test/resources/AppiumLogs/"

									+ deviceName + ".txt"))
							.usingPort(Integer.parseInt(appiumPort)));
		}
		int i = 0;
		for (Map.Entry<String, DeviceBean> entry : DeviceBean.deviceInfo
				.entrySet()) {
			appiumServiceList.get(i).build().start();

			appiumServiceUrl = appiumServiceList.get(i).build().getUrl()
					.toString();
			CreateThread mainThread = new CreateThread();
			mainThread
					.start(entry.getValue().getDeviceName(), appiumServiceUrl);
			i++;
		}
	}

}
