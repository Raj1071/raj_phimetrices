package com.phimetrices.driver;

import io.appium.java_client.android.AndroidDriver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.TestNG;

import utils.load.dataSource.ExcelLib;
import utils.report.template.ExtentReports;
import utils.report.template.GridType;

public class CreateThread extends Thread {
	private Thread t;
	public static volatile String threadName;
	ExcelLib excelData = new ExcelLib();
	public String appiumPort, ipAddress, devicePort, bootStrapPort,
			platformName, androidVersion, deviceName, appPackage, activityName,
			sheetName;
	public static String simCard;
	public String udid;
	public static ExtentReports reports;
	public static AndroidDriver<WebElement> driver = null;
	public static DesiredCapabilities capabilities = new DesiredCapabilities();
	public static String appiumServiceUrl1;
	public static int i;
	int itr;

	public void run() {

		reports = new ExtentReports();
		reports = initializeReport(threadName);
		for (Map.Entry<String, DeviceBean> entry : DeviceBean.deviceInfo
				.entrySet()) {

			appiumPort = entry.getValue().getAppiumPort();
			ipAddress = entry.getValue().getIpAddress();
			devicePort = entry.getValue().getDevicePort();
			bootStrapPort = entry.getValue().getBootStrapPort();
			platformName = entry.getValue().getPlatformName();
			androidVersion = entry.getValue().getAndroidVersion();
			deviceName = entry.getValue().getDeviceName();

			simCard = entry.getValue().getSimCard();
			udid = entry.getValue().getUdid();

			if (threadName.contains(deviceName)) {

				deviceName = entry.getValue().getDeviceName();
				capabilities.setCapability("platformName", platformName);
				capabilities.setCapability("platformVersion", androidVersion);
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability("udid", udid);
				// capabilities.setCapability("unicodeKeyboard", true);
				// capabilities.setCapability("autoWebview", true);
				// capabilities.setCapability("autoLaunch", true);
				try {
					String rowCount = excelData.getExcelData("Iteration", 1, 0,
							"DeviceInfo.xls");
					itr = new Integer(rowCount);
				} catch (Exception e) {
					e.printStackTrace();
				}
				for (i = 1; i <= itr; i++) {
					TestNG runner = new TestNG();
					List<String> suitefiles = new ArrayList<String>();
					suitefiles.add("testng.xml");
					runner.setTestSuites(suitefiles);
					runner.run();
				}
				break;
			}
		}
	}

	public void start(String threadName, String appiumServiceUrl)
			throws Exception {
		System.out
				.println("-------------------start Method--------------------------");
		appiumServiceUrl1 = appiumServiceUrl;
		this.threadName = threadName;
		System.out.println("In start method " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}

	public ExtentReports initializeReport(String threadName) {
		// public static Logger log = Logger.getLogger("devpinoyLogger");
		ExtentReports reports = new ExtentReports();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		Date date = new Date();
		String reportPath = System.getProperty("user.dir")
				+ "/src/test/resources/Reports/PhiMetricsReport_" + threadName
				+ "_" + dateFormat.format(date) + ".html";
		reports.init(reportPath, true);
		reports.config().displayCallerClass(false);
		reports.init(reportPath, true, GridType.MASONRY);
		reports.config().setImageSize("50%");
		reports.config().documentTitle("PhiMetrics Report " + threadName);
		return reports;
	}
}
