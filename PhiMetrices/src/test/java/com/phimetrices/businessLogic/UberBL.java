package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.UberPL;

public class UberBL extends CreateThread{
	UberPL uberPage = PageFactory.initElements(driver, UberPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public void bookRide() throws Exception{
		commonLib.logOnInfo("Uber Cab Booking", "book a ride ");
		Thread.sleep(5000);
		commonLib.click(uberPage.getTabPickup());
		commonLib.click(uberPage.getTabDestination());
		Thread.sleep(2000);
		commonLib.typeText(uberPage.getTxtDestination(), "Shipra Mall");
		Thread.sleep(8000);
		commonLib.click(uberPage.getListedDestination());
//		commonLib.click(uberPage.getBtnScheduleUnderGo());
		commonLib.logOnSuccess("Booking Status", "successfully booked a ride ");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
}
