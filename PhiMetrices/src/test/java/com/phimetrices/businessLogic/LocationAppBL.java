package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.LocationAppPL;

public class LocationAppBL extends CreateThread {

	LocationAppPL lacationAppPage = PageFactory.initElements(driver,
			LocationAppPL.class);
	CommonUtills commonLib = new CommonUtills();

	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	ExcelLib excelData = new ExcelLib();

	public void getSignalsValues(String sheetName, int latColumn,
			int signalColumn, int rowNum, int longColumn) throws Exception {
		commonLib.logOnInfo("Location Test app", "getting the signals value");
		Thread.sleep(5000);
		commonLib.click(lacationAppPage.getBtnRefresh());
		String latLongValue = commonLib.getAttribute(
				lacationAppPage.getTxtLatLongValue(), "name");

		String values[] = latLongValue.split(",");
		excelData.setExcelData(sheetName, rowNum, latColumn, values[0] + "",
				threadName, "ResultSheet.xls");
		excelData.setExcelData(sheetName, rowNum, longColumn, values[1] + "",
				threadName, "ResultSheet.xls");
		System.out
				.println("--------------latLongValue---------" + latLongValue);
		String signalsValue = commonLib.getAttribute(
				lacationAppPage.getTxtSignalValue(), "name");
		
		excelData.setExcelData(sheetName, rowNum, signalColumn, signalsValue
				+ "", threadName, "ResultSheet.xls");

		System.out.println("-----------------signalsValue" + signalsValue);
		commonLib.logOnSuccess("LocationTest app",
				"signals value after execution of test");
		 driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
}
