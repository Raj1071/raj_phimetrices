package com.phimetrices.businessLogic;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.GoogleMapPL;

public class GoogleMapBL extends CreateThread{
	GoogleMapPL googleMapPage= PageFactory.initElements(driver, GoogleMapPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public void loadingTime() throws Exception, IOException{
		commonLib.logOnInfo("Google Map", "Time to get");
		startTime = new Date();
		excelData.setExcelData("GoolgleMap", 1, 1, startTime + "",threadName,"ResultSheet.xls");
		commonLib.click(googleMapPage.getBtnGo());
		commonLib.click(googleMapPage.getTabChooseDestination());
		commonLib.typeText(googleMapPage.getTxtChooseDestination(), "Shipra Mall");
		Date date = new Date();

		System.out.println("------------" + dateFormat.format(date));
		commonLib.click(googleMapPage.getTabListItem());

		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(googleMapPage.getProcessBar());
			Thread.sleep(200);
		}
		Date d2 = new Date();
		long diff = d2.getTime() - date.getTime();
		double diffSeconds = diff / 1000 % 60;
		commonLib.logOnSuccess("Time Status", "getting Time posted successfully");
		excelData.setExcelData("GoolgleMap", 1, 2, diffSeconds + "",threadName,"ResultSheet.xls");
		excelData.setExcelData("GoolgleMap", 1, 3, "Pass",threadName,"ResultSheet.xls");
		
		endTime = new Date();
		excelData.setExcelData("GoolgleMap", 1, 4, endTime + "",threadName,"ResultSheet.xls");
		
	}
}
