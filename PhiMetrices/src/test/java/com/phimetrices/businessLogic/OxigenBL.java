package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;

import java.util.Date;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.load.dataSource.ExcelLib;
import android.content.pm.ApplicationInfo;
import android.os.Build;


import android.webkit.WebView;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.OxigenPL;

public class OxigenBL extends CreateThread {
	String mobileNumber = "9990176197";
	String amount = "10";
	String pin = "701010";
	String cardNumber = "4693751946026418";
	String cardHolderName = "RAJ KUMAR";
	String cvv = "909";
	OxigenPL oxigenPage = PageFactory.initElements(driver, OxigenPL.class);

	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();

	public void transferMoney() throws Exception {
		commonLib.logOnInfo("Oxigen Transfer Started",
				"Sending Money through Oxigen");
		commonLib.waitForPageToLoad();
		commonLib.click(oxigenPage.getBtnSendMoney());
		commonLib.typeText(oxigenPage.getTxtAmount(), amount);
		commonLib.typeText(oxigenPage.getTxtNumber(), mobileNumber);
		commonLib.click(oxigenPage.getBtnSend());
		commonLib.typeText(oxigenPage.getTxtPin(), pin);
		commonLib.click(oxigenPage.getBtnSubmit());
		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Oxigen Trasfer", "Money Transfer sucessfully");
		commonLib.click(oxigenPage.getBtnGreat());
	}

	public void loadMoney() throws Exception {
		commonLib.logOnInfo("Oxigen Load Money Started",
				"Add Money to oxigen Wallet");
//		commonLib.waitForPageToLoad();
		Thread.sleep(6000);
		commonLib.click(oxigenPage.getBtnLoadMoney());
		commonLib.typeText(oxigenPage.getTxtAmountToLoad(), amount);
//		commonLib.waitForPageToLoad();
//		MobileElement dropDownForDebit = (MobileElement) driver
//				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.oxigen.oxigenwallet:id/text_payment_options\")).scrollIntoView(new UiSelector().textContains(\"Debit Card\"))");
//		dropDownForDebit.click();
		commonLib.click(oxigenPage.getBtnDebitCard());
		
//		Thread.sleep(8000);
	

//		commonLib.waitForPageToLoad();
//		commonLib.click(oxigenPage.getBtnSelectBank());
//		MobileElement dropDownForBank = (MobileElement) driver
//				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"android:id/text1\")).scrollIntoView(new UiSelector().textContains(\"ICICI Netbanking\"))");
//		dropDownForBank.click();
//		commonLib.waitForPageToLoad();
//		commonLib.click(oxigenPage.getBtnOkToLoadMoney());
//		commonLib.waitForPageToLoad();
//		commonLib.typeText(oxigenPage.getTxtUserID(), "");
//		commonLib.typeText(oxigenPage.getTxtPassword(), "");
//		commonLib.click(oxigenPage.getBtnPaymentLogin());
//		commonLib.waitForPageToLoad();
//		commonLib.typeText(oxigenPage.getTxtCardValue1(), "");
		
	
		WebDriverWait wait = new WebDriverWait(driver,300);
		By webView = By.className("android.webkit.WebView");//("android.webkit.WebView");
		Set<String> availableContexts1 = driver.getContextHandles();
		System.out.println("Total No of Context Found Before reaching WebView = "+ availableContexts1.size());
		System.out.println("Context Name is "+ availableContexts1);
		
		driver.findElement( By.className("android.webkit.WebView")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(webView));
		
		Set<String> availableContexts = driver.getContextHandles();
		System.out.println("Total No of Context Found After we reach to WebView = "+ availableContexts.size());
		for(String context12 : availableContexts) {
			if(context12.contains("WEBVIEW")){
				System.out.println("Context Name is " + context12);
				// 4.3 Call context() method with the id of the context you want to access and change it to WEBVIEW_1
				//(This puts Appium session into a mode where all commands are interpreted as being intended for automating the web view)
			driver.context(context12);
				break;
			}
		}
//		Thread.sleep(2000);
		commonLib.typeText(oxigenPage.getTxtCardNumber(), cardNumber);
		commonLib.waitForPageToLoad();
		commonLib.typeText(oxigenPage.getTxtCardHolderName(), cardHolderName);
		commonLib.click(oxigenPage.getSelectMonth());

		MobileElement dropDownMonth = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"android:id/text1\")).scrollIntoView(new UiSelector().textContains(\"September(09)\"))");
		dropDownMonth.click();
		commonLib.waitForPageToLoad();
		MobileElement dropDownYear = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"android:id/text1\")).scrollIntoView(new UiSelector().textContains(\"2023\"))");
		dropDownYear.click();
		commonLib.waitForPageToLoad();
		commonLib.typeText(oxigenPage.getTxtCVV(), cvv);
		commonLib.typeText(oxigenPage.getTxtNickName(), cardHolderName);
		commonLib.click(oxigenPage.getBtnOk());
		commonLib.waitForPageToLoad();
		Thread.sleep(15000);
		commonLib.logOnSuccess("Oxigen Load Money status",
				"Money Added sucessfully");
	}
}
