package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.FaceBookPL;
import com.phimetrices.pageRepository.MobileLocationFinderPL;

public class FaceBookBL extends CreateThread {
	String scrollLocator = "com.facebook.katana:id/bookmarks_list";
	String scrollLocatorForVideo = "com.facebook.katana:id/image";
	String logoutLocator = "Log Out";
	String videoLocator = "Video";
	String sheetName = "Facebook";
	String fileName = "ResultSheet.xls";
	String statusPost = "Hello! Good evening.... ";
	FaceBookPL faceBookPage = PageFactory
			.initElements(driver, FaceBookPL.class);

	MobileLocationFinderPL andLocationPage = PageFactory.initElements(driver,
			MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public void login() throws Exception {
		commonLib.logOnInfo("FaceBook Login", "Logging in to the FB app");

		String userName = "uzkennw_bushaksen_1441043317@tfbnw.net";
		String password = "1466503936";
		commonLib.typeText(faceBookPage.getTxtUserName(), userName);
		commonLib.click(faceBookPage.getTxtPassword());
		commonLib.typeText(faceBookPage.getTxtPassword(), password);
		commonLib.click(faceBookPage.getBtnLogin());
		commonLib.logOnSuccess("FaceBook Login", "Logged in to the FB app");
		commonLib.click(faceBookPage.getBtnNotNow());
	}

	public void postStatus(int rowNum) throws Exception {
		commonLib.logOnInfo("Post Status", "Posting new status");
		startTime = new Date();
		excelData.setExcelData("Facebook", rowNum, 1, startTime + "",
				threadName, "ResultSheet.xls");
		
		commonLib.click(faceBookPage.getAreaStatus());
		commonLib.typeText(faceBookPage.getTxtStatus(), statusPost);
		System.out.println("------status size------" + statusPost.length()
				+ "---------------");
		Date date = new Date();
		System.out.println("------------" + dateFormat.format(date));
		commonLib.click(faceBookPage.getBtnPost());

		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(faceBookPage.getLoader());
			Thread.sleep(200);
		}
		System.out.println("---------flag value----------" + flag
				+ "------------------");
		Date d2 = new Date();
		long diff = d2.getTime() - date.getTime();
		// double diffSeconds = diff / 1000 % 60;
		if (diff > 4500) {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, "Pass", threadName,
					"ResultSheet.xls");
		}

		commonLib.logOnSuccess("Post Status", "New status posted successfully");

		// driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

	public void uploadImage(int rowNum) throws Exception {
		commonLib.logOnInfo("Post Image", "Posting new image");
		boolean flag = true;
		commonLib.click(faceBookPage.getAreaStatus());
		commonLib.click(faceBookPage.getTabPhotos());
		 
		 
//		commonLib.click(faceBookPage.getClickTabBookmark());
//		Thread.sleep(6000);
//		List<WebElement> getProfile = faceBookPage.getTabViewProfile();
//		System.out.println("-----------------" + getProfile.size()
//				+ "-----------------------");
//		commonLib.click(getProfile.get(0));

		// commonLib.click(faceBookPage.getTabViewProfile());
		
//		MobileElement scrollToPhotos = (MobileElement) driver
//				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"android:id/list\")).scrollIntoView(new UiSelector().resourceId(\"com.facebook.katana:id/feed_composer_photo_button\"))");
//		scrollToPhotos.click();
//		Thread.sleep(4000);
		
		 commonLib.click(faceBookPage.getTabPhotosVideos());
		List<WebElement> getFirstPhoto = faceBookPage.getFirstPhoto();
		System.out.println("-----------------" + getFirstPhoto.size()
				+ "-----------------------");
		commonLib.click(getFirstPhoto.get(0));
		commonLib.click(faceBookPage.getBtnDone());
		Thread.sleep(2000);
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(faceBookPage.getBtnPost());

		while (flag) {
			flag = commonLib.isDisplayed(faceBookPage.getLoader());
		}
		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();

		if (diff > 7000) {
			excelData.setExcelData(sheetName, rowNum, 5, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 6, "fail", threadName,
					"ResultSheet.xls");
		} else {
			// double diffSeconds = diff / 1000 % 60;
			excelData.setExcelData(sheetName, rowNum, 5, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData("Facebook", rowNum, 6, "Pass", threadName,
					"ResultSheet.xls");
		}
		commonLib.logOnSuccess("Post Image", "New image posted successfully");
		// commonLib.waitForPageToLoad();

	}

	public void uploadVideo(int rowNum) throws Exception {
		commonLib.logOnInfo("Uploading video ", "upload video on facebook");
		boolean flag = true;
		Thread.sleep(2000);
		commonLib.click(faceBookPage.getAreaStatus());
		commonLib.click(faceBookPage.getTabPhotosVideos());
//		commonLib.click(faceBookPage.getClickTabBookmark());
//		List<WebElement> getProfile = faceBookPage.getTabViewProfile();
//		System.out.println("-----------------" + getProfile.size()
//				+ "-----------------------");
//		commonLib.click(getProfile.get(0));
//		MobileElement scrollToPhotos = (MobileElement) driver
//				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"android:id/list\")).scrollIntoView(new UiSelector().resourceId(\"com.facebook.katana:id/feed_composer_photo_button\"))");
//		scrollToPhotos.click();

//		commonLib.click(faceBookPage.getTabPhotos());
//
		Dimension dimension = driver.manage().window().getSize();
		Double screenHeightStart1 = dimension.getHeight() * 0.5;
		int scrollStart1 = screenHeightStart1.intValue();
		Double screenHeightEnd1 = dimension.getHeight() * 0.2;
		int scrollEnd1 = screenHeightEnd1.intValue();
		driver.swipe(0, scrollStart1, 0, scrollEnd1, 2000);

		List<WebElement> getFirstVideo = faceBookPage.getTabVideo();
		System.out.println("-----------------" + getFirstVideo.size()
				+ "-----------------------");
		commonLib.click(getFirstVideo.get(0));

		commonLib.click(faceBookPage.getBtnDone());
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(faceBookPage.getBtnPost());
		while (flag) {
			flag = commonLib.isDisplayed(faceBookPage.getLoader());
		}
		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		if (diff > 7000) {
			excelData.setExcelData("Facebook", rowNum, 7, diff + "",
					threadName, "ResultSheet.xls");
			excelData.setExcelData("Facebook", rowNum, 8, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData("Facebook", rowNum, 7, diff + "",
					threadName, "ResultSheet.xls");
			excelData.setExcelData("Facebook", rowNum, 8, "Pass", threadName,
					"ResultSheet.xls");
		}

		commonLib.logOnSuccess("Post video", "New video posted successfully");
		endTime = new Date();
		excelData.setExcelData("Facebook", rowNum, 9, endTime + "", threadName,
				"ResultSheet.xls");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);

	}

	public void logOut() throws Exception {
		commonLib.logOnInfo("LogOut ", "Logout from Facebook");
		commonLib.waitForPageToLoad();
		commonLib.click(faceBookPage.getClickTabBookmark());
		commonLib.waitForPageToLoad();

		MobileElement scrollToLogOut = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\""
						+ scrollLocator
						+ "\")).scrollIntoView(new UiSelector().textContains(\""
						+ logoutLocator + "\"))");
		scrollToLogOut.click();
		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("FaceBook Logout", "Logged out from the FB app");
		commonLib.waitForPageToLoad();
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

}
