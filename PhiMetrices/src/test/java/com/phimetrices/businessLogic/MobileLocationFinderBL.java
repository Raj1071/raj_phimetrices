package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.MobileLocationFinderPL;

public class MobileLocationFinderBL extends CreateThread {

	MobileLocationFinderPL andLocationPage= PageFactory.initElements(driver, MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	
	public void getLATValue(String sheetName,int latColumn,int longColumn,int rowNum) throws Exception{
		commonLib.logOnInfo("AndLocation app LAT value",
				"getting the LAT value of mobile location");
		Thread.sleep(10000);
		String latValue =commonLib.getAttribute(andLocationPage.getLatValue(), "name");
		System.out.println("------------------LAT Value :"+latValue);
		excelData.setExcelData(sheetName, rowNum, latColumn, latValue + "",threadName,"ResultSheet.xls");
//		commonLib.getAndVerifyTextPresent(andLocationPage.getLatValue(), "someValue");
		String longValue =commonLib.getAttribute(andLocationPage.getLongValue(), "name");
		System.out.println("------------------LONG Value :"+longValue);
		Thread.sleep(2000);
		commonLib.logOnSuccess("Mobile Location Status", "getting Lat And LONG values");
		excelData.setExcelData(sheetName, rowNum, longColumn, longValue + "",threadName,"ResultSheet.xls");
		
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
}
