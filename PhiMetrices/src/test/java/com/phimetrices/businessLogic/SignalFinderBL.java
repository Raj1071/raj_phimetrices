package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.SignalFinderPL;

public class SignalFinderBL extends CreateThread {
	
	SignalFinderPL signalFinderPage = PageFactory.initElements(driver, SignalFinderPL.class);
	
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public void findSignals(){
		commonLib.logOnInfo("Finding Signals", "Getting all signals through DialPad");
		commonLib.click(signalFinderPage.getBtnDialPad());
		
		if(threadName.contains("Samsung"))
		{
			commonLib.click(signalFinderPage.getBtnStar());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnZero());
			commonLib.click(signalFinderPage.getBtnZero());
			commonLib.click(signalFinderPage.getBtnOne());
			commonLib.click(signalFinderPage.getBtnOne());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnDial());
		}else{
			commonLib.click(signalFinderPage.getBtnStar());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnStar());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnFour());
			commonLib.click(signalFinderPage.getBtnSix());
			commonLib.click(signalFinderPage.getBtnThree());
			commonLib.click(signalFinderPage.getBtnSix());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnStar());
			commonLib.click(signalFinderPage.getBtnHash());
			commonLib.click(signalFinderPage.getBtnStar());
			commonLib.click(signalFinderPage.getBtnDial());
			
			commonLib.click(signalFinderPage.getTabPhoneInformation());
		}
		
		
		commonLib.logOnSuccess("Signal Status", "getting all the signal values");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
	

}
