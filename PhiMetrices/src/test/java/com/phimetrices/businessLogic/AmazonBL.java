package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.AmazonPL;

public class AmazonBL extends CreateThread{

	AmazonPL amazonPage= PageFactory.initElements(driver, AmazonPL.class);
	
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	
	public void shopProduct(){
		commonLib.logOnInfo("Welcome to Amazon", "shopping product from Amazon");
//		commonLib.click(amazonPage.getBtnShopBycategory());
		commonLib.click(amazonPage.getTabSearchItem());
		commonLib.typeText(amazonPage.getTabSearchItem(), "Brush");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_ENTER );
		
		
	}
}
