package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.MobileLocationFinderPL;
import com.phimetrices.pageRepository.WhatsAppPL;

public class WhatsAppBL extends CreateThread {
	String contactPerson = "Ankit Singh";
	String contactPersonNxt = "Ankit Singh";

	String sheetName = "Whatsapp";
	String result = "Pass";
	WhatsAppPL whatsappPage = PageFactory
			.initElements(driver, WhatsAppPL.class);
	CommonUtills commonLib = new CommonUtills();
	MobileLocationFinderPL andLocationPage = PageFactory.initElements(driver,
			MobileLocationFinderPL.class);
	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	ExcelLib excelData = new ExcelLib();

	public void sendImage(int rowNum) throws Exception {
		commonLib.logOnInfo("Whatsapp Send", "Send image on chat");
		startTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 1, startTime + "", threadName,
				"ResultSheet.xls");
		// commonLib.waitForPageToLoad();

		commonLib.click(whatsappPage.getIconSearch());
		commonLib.typeText(whatsappPage.getTxtSearch(), contactPerson);
		// commonLib.waitForPageToLoad();
		commonLib.click(whatsappPage.getSelectUser());
		// commonLib.waitForPageToLoad();
		commonLib.click(whatsappPage.getClickAttach());
		commonLib.click(whatsappPage.getClickGallery());
		commonLib.click(whatsappPage.getClickPhotosTab());
		commonLib.click(whatsappPage.getClickCameraPhotos());
		commonLib.waitForPageToLoad();
		List<WebElement> getFirstImage = whatsappPage.getFirstPhoto();
		System.out.println("-----------------" + getFirstImage.size()
				+ "-----------------------");
		commonLib.click(getFirstImage.get(0));
		// commonLib.waitForPageToLoad();
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(whatsappPage.getClickSendButtonOfImage());
		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(whatsappPage.getUploadProcessBar());
			Thread.sleep(200);
		}
		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------" + flag);
		if (diff > 5000) {
			excelData.setExcelData(sheetName, rowNum, 2, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 3, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 2, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 3, result, threadName,
					"ResultSheet.xls");
		}

		// commonLib.waitForPageToLoad();
		commonLib
				.logOnSuccess("Upload Status", "New image posted successfully");
	}

	public void sendVideo(int rowNum) throws Exception {
		commonLib.logOnInfo("Whatsapp Send", "Send Video on chat");
		// commonLib.waitForPageToLoad();
		commonLib.click(whatsappPage.getClickAttach());
		commonLib.click(whatsappPage.getClickGallery());
		commonLib.click(whatsappPage.getClickVideosTab());
		commonLib.click(whatsappPage.getClickAllVideos());
		// commonLib.waitForPageToLoad();
		List<WebElement> getFirstVideo = whatsappPage.getFirstVideo();
		System.out.println("-----------------" + getFirstVideo.size()
				+ "-----------------------");
		commonLib.click(getFirstVideo.get(0));
		// commonLib.waitForPageToLoad();
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(whatsappPage.getClickSendButtonOfVideos());
		boolean flag = true;

		while (flag) {
			flag = commonLib.isDisplayed(whatsappPage.getUploadProcessBar());
			Thread.sleep(200);
		}
		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------" + flag);
		if (diff > 5000) {
			excelData.setExcelData(sheetName, rowNum, 4, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 5, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 4, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 5, result, threadName,
					"ResultSheet.xls");
		}

		// commonLib.waitForPageToLoad();
		commonLib
				.logOnSuccess("Upload Status", "New Video posted successfully");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

	public void downloadImage(int rowNum) throws Exception {
		commonLib.logOnInfo("Whatsapp Download", "Download Image on chat");
		// commonLib.waitForPageToLoad();
		commonLib.click(whatsappPage.getClickNavigateBack());
		commonLib.click(whatsappPage.getIconSearch());
		commonLib.typeText(whatsappPage.getTxtSearch(), contactPersonNxt);
		// commonLib.waitForPageToLoad();
		commonLib.click(whatsappPage.getSelectUser());
		// commonLib.waitForPageToLoad();
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(whatsappPage.getDownloadImage());

		boolean flag = true;

		while (flag) {

			flag = commonLib.isDisplayed(whatsappPage.getLockDisplayed());
			Thread.sleep(200);
		}

		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------" + flag);
		if (diff > 6000) {
			excelData.setExcelData(sheetName, rowNum, 6, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 7, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 6, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 7, result, threadName,
					"ResultSheet.xls");
		}

		commonLib.logOnSuccess("Download Status",
				"New Image downloaded successfully");
	}

	public void downloadVideo(int rowNum) throws Exception {
		commonLib.logOnInfo("Whatsapp Download", "Download Video on chat");
		// commonLib.waitForPageToLoad();
		// commonLib.click(whatsappPage.getClickNavigateBack());
		// commonLib.click(whatsappPage.getIconSearch());
		// commonLib.typeText(whatsappPage.getTxtSearch(), contactPersonNxt);
		// commonLib.waitForPageToLoad();
		// commonLib.click(whatsappPage.getSelectUser());
		// commonLib.waitForPageToLoad();
		Thread.sleep(2000);
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(whatsappPage.getDownloadVideo());
		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(whatsappPage.getLockVideoDisplayed());
			Thread.sleep(200);
		}
		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------" + flag);
		if (diff > 6000) {
			excelData.setExcelData(sheetName, rowNum, 8, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 9, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 8, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 9, result, threadName,
					"ResultSheet.xls");
		}

		// commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Download Status",
				"New Video downloaded successfully");

		// commonLib.waitForPageToLoad();
		endTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 10, endTime + "", threadName,
				"ResultSheet.xls");
	}

}
