package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.SendingMessagePL;

public class SendingMessageBL extends CreateThread{
	String contactNumber ="9909";
	String composeMessage="Hi! Ready to be executed..";
	SendingMessagePL smsPage= PageFactory.initElements(driver, SendingMessagePL.class);
	CommonUtills commonLib= new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public void sendingSMS(){
		commonLib.logOnInfo("SMS mode", "Sending a text message");
		commonLib.click(smsPage.getBtnComposeSMS());
		commonLib.typeText(smsPage.getTxtnumber(), contactNumber);
		commonLib.typeText(smsPage.getTxtComposeMessage(), composeMessage);
		commonLib.click(smsPage.getBtnSendMessage());
		commonLib.logOnSuccess("SMS Status", "successfully send a SMS");
		
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
	
}
