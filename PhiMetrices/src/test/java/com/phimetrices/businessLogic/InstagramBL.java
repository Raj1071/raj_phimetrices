package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.InstagramPL;
import com.phimetrices.pageRepository.MobileLocationFinderPL;

public class InstagramBL extends CreateThread {
	String messageCaption = "Test case executed";
	String username = "";
	String password = "";
	String sheetName = "Instagram";
	String result = "Pass";
	String scrollLocator = "android:id/list";
	String logoutLocator = "Log Out";
	InstagramPL instagramPage = PageFactory.initElements(driver,
			InstagramPL.class);

	MobileLocationFinderPL andLocationPage = PageFactory.initElements(driver,
			MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	ExcelLib excelData = new ExcelLib();

	public void login() throws Exception {
		commonLib.logOnInfo("Instagram Login", "Login to Instagram");
		startTime = new Date();
		commonLib.waitForPageToLoad();
		commonLib.typeText(instagramPage.getTxtUsername(), username);
		commonLib.typeText(instagramPage.getTxtPassword(), password);
		commonLib.click(instagramPage.getBtnLogin());
		commonLib.logOnSuccess("login Succesfully", "Logged In successfully");
	}

	public void uploadImage(int rowNum) throws Exception {
		commonLib.logOnInfo("Instagram Post", "Post image on Instagram");
		startTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 1, startTime + "",
				threadName, "ResultSheet.xls");
		Thread.sleep(3000);
		System.out.println("****************Device Name*************"+threadName+".......");
		commonLib.click(instagramPage.getIconCamera());
		commonLib.click(instagramPage.getBtnGallery());
		commonLib.click(instagramPage.getBtnDownload());
		List<WebElement> getImage = instagramPage.getFirstPhoto();
		System.out.println("-----------------" + getImage.size()
				+ "-----------------------");
		commonLib.click(getImage.get(0));
		commonLib.click(instagramPage.getTabNext());
		commonLib.click(instagramPage.getTabNext());

		commonLib.typeText(instagramPage.getTxtCaption(), messageCaption);

		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		Thread.sleep(2000);
		commonLib.click(instagramPage.getTabNextAgain());
		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(instagramPage.getUploadingStatus())
					|| commonLib.isDisplayed(instagramPage
							.getAfterUploadStatus());
			Thread.sleep(200);
		}
		Date d2 = new Date();
		System.out.println("------------" + dateFormat.format(d2)
				+ "----------" + flag);
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------");
		if (diff > 6500) {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, result, threadName,
					"ResultSheet.xls");
		}
		commonLib.logOnSuccess("Post Image", "New image posted successfully");
		endTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 5, endTime + "", threadName,
				"ResultSheet.xls");
		System.out.println("****************threadName*********" + threadName
				+ ("this"));
		Thread.sleep(3000);
//		commonLib.pressHomeKey();
		 driver.longPressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

	public void logOut() throws Exception {
		commonLib.logOnInfo("Instagram LogOut", "LogOut code");
		startTime = new Date();
		commonLib.waitForPageToLoad();
		commonLib.click(instagramPage.getIconProfile());
		commonLib.click(instagramPage.getTabOption());
		commonLib.waitForPageToLoad();
		MobileElement scrollToLogOut = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\""
						+ scrollLocator
						+ "\")).scrollIntoView(new UiSelector().textContains(\""
						+ logoutLocator + "\"))");
		scrollToLogOut.click();
		commonLib.click(instagramPage.getBtnConfirmLogout());
		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Logout Status",
				"Logged out from Instagram successfully");
	}

}
