package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.FlipkartPL;

public class FlipkartBL extends CreateThread {
	
	FlipkartPL flipkartPage =PageFactory.initElements(driver, FlipkartPL.class);
	
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public void purchaseProduct() throws Exception{
		
		
		
		commonLib.logOnInfo("Welcome to Flipkart", "Purchase an item from Flipkart");
		commonLib.click(flipkartPage.getSearchBox());
		commonLib.typeText(flipkartPage.getTxtSearch(), "Pencil");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_ENTER );
		Thread.sleep(8000);
		commonLib.click(flipkartPage.getTabItem());
		
		MobileElement scrollToLogOut = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()"
						+ ".resourceId(\"com.flipkart.android:id/product_summary_v2_1\")).scrollIntoView(new UiSelector().textContains(\"BUY NOW\"))");
		scrollToLogOut.click();
		
//		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
}
