package com.phimetrices.businessLogic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.MobileLocationFinderPL;
import com.phimetrices.pageRepository.TwitterPL;

public class TwitterBL extends CreateThread {
	String username = "test1@kratikal.com";
	String password = "test1@kratikal";
	String message = "Welcome Post Updated";
	String sheetName = "Twitter";
	String result = "Pass";
	TwitterPL twitterPage = PageFactory.initElements(driver, TwitterPL.class);
	MobileLocationFinderPL andLocationPage = PageFactory.initElements(driver,
			MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();

	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	ExcelLib excelData = new ExcelLib();

	public void login() throws Exception {
		commonLib.logOnInfo("Twitter LogIn", "LogIn to Twitter");
		startTime = new Date();
		commonLib.waitForPageToLoad();
		commonLib.click(twitterPage.getBtnSignIn());
		commonLib.typeText(twitterPage.getTxtUsername(), username);
		commonLib.waitForPageToLoad();
		commonLib.typeText(twitterPage.getTxtPassword(), password);
		commonLib.click(twitterPage.getBtnLogin());
		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Twitter Login", "Logged In successfully");
	}

	public void scrollWall() throws InterruptedException {
		Thread.sleep(5000);
		Dimension dimensions = driver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * 0.5;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * 0.2;
		int scrollEnd = screenHeightEnd.intValue();
		int duration = 0;
		driver.swipe(0, scrollStart, 0, scrollEnd, duration);
		Thread.sleep(2000);
		Dimension dimension = driver.manage().window().getSize();
		Double screenHeightStartPoint = dimension.getHeight() * 0.5;
		int scrollStartAgain = screenHeightStartPoint.intValue();
		Double screenHeightEndPoint = dimensions.getHeight() * 0.2;
		int scrollEndAgain = screenHeightEndPoint.intValue();
		driver.swipe(0, scrollStartAgain, 0, scrollEndAgain, duration);
		// Connection value=driver.getConnection();
		// System.out.println("-------------connection Value"+Connection.DATA);
	}

	public void postStatus(int rowNum) throws Exception {
		commonLib.logOnInfo("Twitter status", "Status post on Twitter");
		startTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 1, startTime + "", threadName,
				"ResultSheet.xls");
		commonLib.click(twitterPage.getBtnCompose());
		commonLib.click(twitterPage.getBtnGallery());
		Thread.sleep(2000);
		commonLib.click(twitterPage.getTabGallery());
		commonLib.click(twitterPage.getTabWhatsappImages());
		Thread.sleep(2000);
		List<WebElement> getFirstPhoto = twitterPage.getFirstPhoto();
		System.out.println("-----------------" + getFirstPhoto.size()
				+ "-----------------------");
		commonLib.click(getFirstPhoto.get(0));
		
//		commonLib.click(twitterPage.getListGallery());
		commonLib.typeText(twitterPage.getTxtStatus(), message);

		Date d1 = new Date();
		commonLib.click(twitterPage.getBtnTweet());
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(twitterPage.getTabHome());

		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(twitterPage.getUploadStatus());
			Thread.sleep(100);
		}
		Date newDate = new Date();
		System.out.println("------------" + dateFormat.format(newDate)
				+ "----------" + flag);
		long diff = newDate.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------");
		if (diff > 7000) {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, result, threadName,
					"ResultSheet.xls");
		}
		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Twitter Status Post",
				"Status posted on Twitter successfully");
		endTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 5, endTime + "", threadName,
				"ResultSheet.xls");
//		excelData.setExcelData(sheetName, rowNum, 6, threadName + "", threadName,
//				"ResultSheet.xls");
		Thread.sleep(5000);
		commonLib.pressHomeKey();

	}

	public void logout() throws Exception {
		commonLib.logOnInfo("Twitter status", "Status post on Twitter");
		startTime = new Date();
		commonLib.waitForPageToLoad();
		commonLib.click(twitterPage.getTabNavigateToLogout());
		commonLib.waitForPageToLoad();
		commonLib.click(twitterPage.getBtnSettingAndPrivacy());
		commonLib.waitForPageToLoad();
		commonLib.click(twitterPage.getBtnLogout());
		commonLib.waitForPageToLoad();
		commonLib.click(twitterPage.getBtnLogoutOK());
		commonLib.logOnSuccess("Twitter Logout",
				"Logout from Twitter successfully");

	}

}
