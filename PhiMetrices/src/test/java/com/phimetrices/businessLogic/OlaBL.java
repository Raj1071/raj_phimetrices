package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.OlaPL;

public class OlaBL extends CreateThread {

	OlaPL olaPage = PageFactory.initElements(driver, OlaPL.class);
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public void bookRide() throws Exception {
		commonLib.logOnInfo("Ola Cab", "Booking a ride");
		Thread.sleep(8000);
		commonLib.click(olaPage.getBtnMicroCar());
		commonLib.click(olaPage.getTabDropLocation());
		commonLib.typeText(olaPage.getTxtDropLocation(), "Shipra Mall");
		Thread.sleep(4000);
		commonLib.click(olaPage.getTabFirstList());
	
		commonLib.click(olaPage.getBtnRideNow());
		// commonLib.click(olaPage.getBtnConfirmBooking());
		commonLib.logOnSuccess("Booking Status", "successfully booked a ride ");
		 driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
}
