package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.LinkedInPL;
import com.phimetrices.pageRepository.MobileLocationFinderPL;

public class LinkedInBL extends CreateThread {
	String article = "http://www.seleniumhq.org";
	String postMessage = "Get the Best Automation tutorial from this site";
	String emailId = "";
	String password = "";
	String scrollLocator = "com.linkedin.android:id/settings_row_view_container";
	String logoutLocator = "Sign out";
	String sheetName = "Linkedin";
	String result = "Pass";
	LinkedInPL linkedInPage = PageFactory
			.initElements(driver, LinkedInPL.class);

	MobileLocationFinderPL andLocationPage = PageFactory.initElements(driver,
			MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();

	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	ExcelLib excelData = new ExcelLib();

	public void login() throws Exception {
		commonLib.logOnInfo("LinkedIn LogIn", "LogIn to LinkedIn");
		commonLib.waitForPageToLoad();
		commonLib.click(linkedInPage.getClickSignIn());
		commonLib.click(linkedInPage.getTabNoneOfTheAboveMail());
		commonLib.typeText(linkedInPage.getTxtEmailId(), emailId);
		commonLib.typeText(linkedInPage.getTxtPassword(), password);

		commonLib.click(linkedInPage.getClickBtnSignIn());
		commonLib.logOnSuccess("Sign In LinkedIn", "Logged In successfully");
	}

	public void updatePost(int rowNum) throws Exception {
		commonLib.logOnInfo("LinkedIn Post", "Post message on LinkedIn");
		startTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 1, startTime + "", threadName,
				"ResultSheet.xls");
		commonLib.click(linkedInPage.getClickIconShare());
		commonLib.typeText(linkedInPage.getTxtAboutPost(), postMessage);

		commonLib.click(linkedInPage.getBtnCamera());
		commonLib.click(linkedInPage.getBtnChoosePhoto());
		Thread.sleep(2000);
		List<WebElement> listImage = linkedInPage.getSelectImage();
		System.out.println("------------------Length of Image"
				+ listImage.size() + "-----------------");
		commonLib.click(listImage.get(0));
		
//		List<WebElement> listImage1 = linkedInPage.getSelectImage2();
//		System.out.println("------------------Length of Image"
//				+ listImage1.size() + "-----------------");
//		commonLib.click(listImage1.get(0));
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(linkedInPage.getClickButtonPost());
		commonLib.click(linkedInPage.getTabHome());
		boolean flag = true;

		while (flag) {
			flag = commonLib.isDisplayed(linkedInPage.getUploadStatus());
			Thread.sleep(200);
		}

		Date d2 = new Date();
		long diff = d2.getTime() - d1.getTime();
		// double diffSeconds = diff / 1000 % 60;
		System.out.println("----------------------" + diff
				+ "---------------------------------" + flag);
		if (diff > 5000) {
			excelData.setExcelData(sheetName,rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName,rowNum, 4, "fail", threadName,
					"ResultSheet.xls");
		} else {
			excelData.setExcelData(sheetName, rowNum, 3, diff + "", threadName,
					"ResultSheet.xls");
			excelData.setExcelData(sheetName, rowNum, 4, result, threadName,
					"ResultSheet.xls");
		}

		commonLib.logOnSuccess("Post Status on LinkedIn",
				"New Status posted successfully");
		endTime = new Date();
		excelData.setExcelData(sheetName, rowNum, 5, endTime + "", threadName,
				"ResultSheet.xls");
	
		
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
//		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

	public void logOut() throws Exception {
		commonLib.logOnInfo("LinkedIn LogOut", "LogOut Execution started");
		startTime = new Date();
		commonLib.waitForPageToLoad();
		commonLib.click(linkedInPage.getBtnProfileLauncher());
		commonLib.waitForPageToLoad();
		commonLib.click(linkedInPage.getBtnSetting());
		commonLib.click(linkedInPage.getBtnCommunications());

		MobileElement scrollToLogOut = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\""
						+ scrollLocator
						+ "\")).scrollIntoView(new UiSelector().textContains(\""
						+ logoutLocator + "\"))");
		scrollToLogOut.click();

		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("LogOut from LinkedIn",
				"successfully logged out from LinkedIn ");
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}

}
