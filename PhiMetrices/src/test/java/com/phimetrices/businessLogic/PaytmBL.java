package com.phimetrices.businessLogic;

import io.appium.java_client.MobileElement;

import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.PaytmPL;

public class PaytmBL extends CreateThread {
	String mobileNumber = "9990176197";
	String amount = "1";
	String description = "money is being transferd";
	String cardNumber = "";
	String cvv = "";

	String scrollMonthLocator = "android:id/text1";
	String monthLocator = "09";
	String scrollYearLocator = "android:id/text1";
	String yearLocator = "2023";
	PaytmPL paytmPage = PageFactory.initElements(driver, PaytmPL.class);

	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();

	public void transferMoney() throws Exception {
		commonLib.logOnInfo("Paytm Transfer Status",
				"Sending money through Paytm");
//		commonLib.waitForPageToLoad();
		Thread.sleep(3000);
		commonLib.click(paytmPage.getBtnPay());
		commonLib.click(paytmPage.getBtnClose());
		commonLib.click(paytmPage.getTabMobileNumber());
		commonLib.typeText(paytmPage.getTxtMobileNumber(), mobileNumber);
		commonLib.typeText(paytmPage.getTxtAmount(), amount);
		commonLib.typeText(paytmPage.getTxtDescription(), description);
//		commonLib.waitForPageToLoad();
		commonLib.click(paytmPage.getBtnProceed());
		Date d1 = new Date();
		System.out.println("------Time before confirmation is proceed------ "
				+ d1.getTime());
		commonLib.click(paytmPage.getBtnConfirmProceed());
		System.out.println("------Time After confirmation is done------ "
				+ d1.getTime());
//		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Paytm Trasfer", "Money Transfer sucessfully");
	}

	public void addMoney() throws Exception {
		commonLib.logOnInfo("Paytm Add Money Status",
				"Adding money to paytm wallet");
//		commonLib.waitForPageToLoad();
		commonLib.click(paytmPage.getBtnHome());
		commonLib.click(paytmPage.getTabAddMoney());
		commonLib.typeText(paytmPage.getTxtAddAmount(), amount);
		commonLib.click(paytmPage.getBtnAddMoney());
//		commonLib.waitForPageToLoad();
		commonLib.typeText(paytmPage.getTxtCardNumber(), cardNumber);
//		commonLib.waitForPageToLoad();
		commonLib.click(paytmPage.getDropDownMonth());

		MobileElement dropDownMonth = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\""
						+ scrollMonthLocator
						+ "\")).scrollIntoView(new UiSelector().textContains(\""
						+ monthLocator + "\"))");
		dropDownMonth.click();

//		commonLib.waitForPageToLoad();
		commonLib.click(paytmPage.getDropDownYear());
		MobileElement dropDownYear = (MobileElement) driver
				.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\""
						+ scrollYearLocator
						+ "\")).scrollIntoView(new UiSelector().textContains(\""
						+ yearLocator + "\"))");
		dropDownYear.click();
//		commonLib.waitForPageToLoad();
		commonLib.typeText(paytmPage.getTxtCVVNumber(), cvv);
		commonLib.click(paytmPage.getBtnPayNow());
//		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("Paytm AddMoney Sucess",
				"sucessfully Added money to paytm wallet");
	}
}
