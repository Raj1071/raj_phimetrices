package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.DropBoxPL;
import com.phimetrices.pageRepository.MobileLocationFinderPL;

public class DropBoxBL extends CreateThread {

	String username = "";
	String password = "";

	DropBoxPL dropBoxPage = PageFactory.initElements(driver, DropBoxPL.class);
	MobileLocationFinderPL andLocationPage= PageFactory.initElements(driver, MobileLocationFinderPL.class);
	CommonUtills commonLib = new CommonUtills();

	Date startTime, endTime;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	ExcelLib excelData = new ExcelLib();
	
	public void login() throws Exception{
		commonLib.logOnInfo("DropBox Login", "Login into the DropBox");
		commonLib.click(dropBoxPage.getLinkSignIn());
		List<WebElement> getUserDetails = dropBoxPage.getTxtUsername();
		System.out.println("-----------------" + getUserDetails.size()
				+ "-----------------------");
		commonLib.typeText(getUserDetails.get(0), username);
		commonLib.typeText(dropBoxPage.getTxtPassword(), password);
		commonLib.click(dropBoxPage.getBtnSignIn());
		commonLib.logOnSuccess("DropBox Login",
				"successfully Login to DropBox");
		
	}
	
	public void uploadFile(int rowNum) throws Exception{
		commonLib.logOnInfo("DropBox Upload File", "Upload file into DropBox");
		startTime = new Date();
		excelData.setExcelData("Dropbox", rowNum, 1, startTime + "",threadName,"ResultSheet.xls");
		Thread.sleep(2000);
		commonLib.click(dropBoxPage.getBtnAttach());
		commonLib.click(dropBoxPage.getTabUploadFiles());
		commonLib.click(dropBoxPage.getTabDownloads());
		List<WebElement> getFile = dropBoxPage.getSelectedFile();
		System.out.println("-----------------" + getFile.size()
				+ "-----------------------");
		Date d1 = new Date();
		System.out.println("------------" + dateFormat.format(d1));
		commonLib.click(getFile.get(0));
		commonLib.click(dropBoxPage.getBtnReplace());
		boolean flag = true;
		while (flag) {
			flag = commonLib.isDisplayed(dropBoxPage.getProcessStatus()
					);
			Thread.sleep(100);
			Date newDate=new Date();
			System.out.println("------------"
					+ dateFormat.format(newDate) + "----------" + flag);
			long diff = newDate.getTime() - d1.getTime();
			double diffSeconds = diff / 1000 % 60;
			System.out.println("----------------------" + diffSeconds
					+ "---------------------------------");
			excelData.setExcelData("Dropbox", rowNum, 2, diffSeconds + "",threadName,"ResultSheet.xls");
			excelData.setExcelData("Dropbox", rowNum, 3, "Pass",threadName,"ResultSheet.xls");
		}
//		commonLib.waitForPageToLoad();
		commonLib.logOnSuccess("DropBox Upload File",
				"File successfully Uploaded to DropBox");
//		commonLib.waitForPageToLoad();
		endTime = new Date();
		excelData.setExcelData("Dropbox", rowNum, 4, endTime + "",threadName,"ResultSheet.xls");
//		commonLib.waitForPageToLoad();
		driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
	}
	
}
