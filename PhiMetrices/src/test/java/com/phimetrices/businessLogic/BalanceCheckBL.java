package com.phimetrices.businessLogic;

import io.appium.java_client.android.AndroidKeyCode;

import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import utils.load.dataSource.ExcelLib;

import com.phimetrices.commonFunction.CommonUtills;
import com.phimetrices.driver.CreateThread;
import com.phimetrices.pageRepository.BalanceCheckPL;

public class BalanceCheckBL extends CreateThread{
	BalanceCheckPL balanceCheckPage= PageFactory.initElements(driver, BalanceCheckPL.class);
	
	CommonUtills commonLib = new CommonUtills();
	Date startTime, endTime;
	ExcelLib excelData = new ExcelLib();
	

	public void balanceCheck() throws Exception{
		commonLib.logOnInfo("Balance Enquiry", "Get the current balance");
		commonLib.click(balanceCheckPage.getBtnDialPad());
		System.out.println("----------------------------------------"+simCard+"------------------------------------------------");
		if(simCard.contains("Airtel")){
			commonLib.click(balanceCheckPage.getBtnStar());
			commonLib.click(balanceCheckPage.getBtnOne());
			commonLib.click(balanceCheckPage.getBtnTwo());
			commonLib.click(balanceCheckPage.getBtnThree());
			commonLib.click(balanceCheckPage.getBtnHash());
			commonLib.click(balanceCheckPage.getBtnDial());
			Thread.sleep(2000);
			commonLib.logOnSuccess("Balance Enquiry", "Succesfully getting the balance");
			driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
		}else if(simCard.contains("Vodafone")){
			commonLib.click(balanceCheckPage.getBtnStar());
			commonLib.click(balanceCheckPage.getBtnOne());
			commonLib.click(balanceCheckPage.getBtnFour());
			commonLib.click(balanceCheckPage.getBtnOne());
			commonLib.click(balanceCheckPage.getBtnHash());
			commonLib.click(balanceCheckPage.getBtnDial());
			Thread.sleep(2000);
			commonLib.logOnSuccess("Balance Enquiry", "Succesfully getting the balance");
			driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
		}else if(simCard.contains("Idea")){
			commonLib.click(balanceCheckPage.getBtnStar());
			commonLib.click(balanceCheckPage.getBtnOne());
			commonLib.click(balanceCheckPage.getBtnThree());
			commonLib.click(balanceCheckPage.getBtnZero());
			commonLib.click(balanceCheckPage.getBtnHash());
			commonLib.click(balanceCheckPage.getBtnDial());
			Thread.sleep(2000);
			commonLib.logOnSuccess("Balance Enquiry", "Succesfully getting the balance");
			driver.pressKeyCode(AndroidKeyCode.KEYCODE_HOME);
		}
	}
}
