package utils.load.dataSource;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelLib {
//	String excelPath = "src/test/resources/Excel/DeviceInfo.xls";
//	String setExcelPath="src/test/resources/Excel/ResultSheet.xls";
	String path ="src/test/resources/Excel/";
	public String getExcelData(String sheetName, int rowNum, int colNum, String fileName)
			throws InvalidFormatException, IOException {

		FileInputStream fis = new FileInputStream(path+fileName);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		Row row = sh.getRow(rowNum);
		Cell c = row.getCell(colNum);
		c.setCellType(Cell.CELL_TYPE_STRING);
		String data = c.getStringCellValue();
		data = data.toString();
		return data;
		// String data = row.getCell(colNum).getStringCellValue();
		//
		// return data;
	}

	public String getDateExcelData(String sheetName, int rowNum, int colNum, String fileName)
			throws InvalidFormatException, IOException {

		FileInputStream fis = new FileInputStream(path+fileName);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		Row row = sh.getRow(rowNum);
		Cell c = row.getCell(colNum);
		DataFormatter fmt = new DataFormatter();

		String data = fmt.formatCellValue(c);

		return data;
	}

	public Row getRow(String sheetName, int rowNum, String fileName)
			throws InvalidFormatException, IOException {
		FileInputStream fis = new FileInputStream(path+fileName);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		Row row = sh.getRow(rowNum);
		return row;
	}

	public int getRowCount(String sheetName, String fileName) throws InvalidFormatException,
			IOException {

		FileInputStream fis = new FileInputStream(path+fileName);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		int rowCount = sh.getLastRowNum();
		return rowCount + 1;
	}

	public synchronized void setExcelData(String sheetName, int rowNum, int colNum,
			String data,String deviceName, String fileName) throws InvalidFormatException, IOException {
		  FileInputStream fis = new FileInputStream(path+fileName);
		  Workbook wb = WorkbookFactory.create(fis);
		  Sheet sh = wb.getSheet(sheetName);
		  Row row = sh.getRow(rowNum);
		  System.out.println("**********************"+deviceName+"****************************");
		  System.out.println("**********************"+rowNum+"****************************");
		  if(row == null) {
			   row= sh.createRow(rowNum);
		  }
		  Cell cel = row.getCell(colNum);
		  if (cel == null) {
		   cel = row.createCell(colNum);
		  }
		  cel.setCellType(Cell.CELL_TYPE_STRING);
		  cel.setCellValue(data);
		  FileOutputStream fos = new FileOutputStream(path+fileName);
		  wb.write(fos);
	
		
		
	}

}
